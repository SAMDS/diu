Bloc 1 - Partie Web : CSS
====================================================================

DIU Enseigner l'informatique au lycée

Univ. Poitiers, Canopé, AEFE

Dans chacun des dossiers [Exercice1](./Exercice1), [Exercice2](./Exercice2) et [Execice3](./Exercice3), vous trouverez des fichiers HTML et des feuilles de style à compléter en suivant les consignes données en commentaires. Dans les fichiers HTML des deux derniers répertoires, il vous faut aussi ajouter la balise qui permet de lier le fichier de style au fichier HTML. Dans ces deux répertoires sont aussi présentes des captures d'écran de ce à quoi devraient ressembler vos pages HTML après avoir complété le style.


