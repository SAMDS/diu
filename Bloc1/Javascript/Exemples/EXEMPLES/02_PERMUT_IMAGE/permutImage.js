/*
 * Explications
 * Il faut garder en mémoire le numéro de l'image courante, pour cela
 * on utilise une variable globale.
 * Note : l'utilisation d'une variable globale est généralement
 *        déconseillé ; mais cela nous facilite la vie ici.
 *
 * Variables globales :
 *   - num : 0 ou 1 ; change de valeur à chaque clic
 *   - images : tableau, numéroté de 0 à 1, contient les deux noms de fichiers
 * Il est assez facile d'étendre le code pour une permutation entre trois
 * images ou plus.
 */
 
 // on suppose que l'image numéro 0 est affichée au chargement de la page
 let num = 0;
 let images = ['bac.jpg', 'bac_modif.jpg'];
 
// cf. exercice précédent pour les explications sur le paramètre et son utilisation
function change(image)
{
	// on change la valeur de la variable "num"
	num = 1 - num;
	// il suffit donc de changer l'attribut "src"
	image.src = images[num];
}
