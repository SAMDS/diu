/*
 * Explications
 * - Le paramètre "element" est l'élément ayant reçu l'événement
 * - On récupère son id (c'est la seule utilité de l'élément)
 * - On concatène "_cpt" pour avoir l'id du compteur
 * - On lui ajoute 1
 * Note : la valeur d'un élément "input" est en mode texte, la
 *        fonction "parseInt" la convertit en entier
 * Note : pour les radio boutons, on se sert de l'attribut "name"
 *        car chaque bouton a son propre id
 */
 
function majCompteur(element)
{
	// construction id du compteur
	let id;
	if (element.type == "radio")
		id = element.name;
	else
		id = element.id;
	id += "_cpt";
	
	// on récupère la caleur courante
	let compteur = document.getElementById(id);
	// incrémentation
	let val = parseInt(compteur.value, 10) + 1;
	// on vérifie que le compteur n'est pas corrompu
	if (isNaN(val))
		val = "pb compteur"
	// on injecte la nouvelle valeur
	compteur.value = val;
}
