/*
 * Explications communes aux deux versions
 * Lorsqu'on récupère l'image, c'est un abus de langage, en réalité
 * on récupère l'élément complet (i.e. l'objet qui correspond à la balise),
 * c'est à dire :
 *   - les attributs (dont "src" qui nous intéresse)
 *   - les propriétés CSS (accessibles également via l'attribut style et ses
 *     propres attributs)
 *   - la zone d'affichage à l'écran (si on voulait dessiner par exemple)
 * Ici il suffit de changer l'attribut "src" pour changer l'image affichée
 */

/*
 * Version 1
 *
 * La fonction reçoit l'élément image à modifier directement en paramètre,
 * ce qui évite la récupération faite dans la version 2.
 * Cf. fichier html pour expliquer comment on appelle cette fonction avec le
 * bon paramètre
 */
function change1(image)
{
	//window.alert("version 1");
	// il suffit donc de changer l'attribut "src"
	image.src = "bac_modif.jpg";
}

/*
 * Version 2
 *
 * Aucun paramètre n'est passé, il faut donc rechercher (dans le DOM) à la main
 * l'élément voulu.
 * La manière la plus simple, car l'élément à un id (qui est unique dans la
 * page), est d'utiliser la fonction JavaScript qui récupère un élément grâce
 * à son id : getElementById
 * Ensuite le code est identique à celui de la version 1
 */
function change2()
{
	//window.alert("version 2");
	let image = document.getElementById("imgbac");
	// plutôt que de réécrire le code, on appelle la fonction de la version 1
	change1(image);
}
