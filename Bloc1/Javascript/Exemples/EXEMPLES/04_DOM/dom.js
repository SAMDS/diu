/*
 * Principe
 * On crée tous les éléments les uns après les autres (balises et noeuds
 * de texte), et on les raccroche.
 * l'ordre de création n'a pas d'importance.
 */

function ajout()
{
	/*
	 * récupération des données
	 */
	// écriture compacte : on enchaine les appels à getElementById et value
	let texte = document.getElementById('texte').value;
	// récupération du nombre de lignes ; ne pas oublier le parseInt
	let nbLignes = document.getElementById('qte').value;
	nbLignes = parseInt(nbLignes);

	/*
	 * création du div à ajouter
	 */
	let div = document.createElement("div");
	// avec l'attribut "class" pour le style CSS
	// On note que le champ s'appelle "className" et non pas "class" : à cause
	// du mot-clé "class" du langage.
	div.className = "ajout";

	/*
	 * création des paragraphes
	 */
	for (let i = 0; i < nbLignes; i++)
	{
		// création paragraphe
		let p = document.createElement("p");
		// création du noeud texte
		let textNode = document.createTextNode("(" + i + ") " + texte);
		// ajout en à la fin du paragraphe
		p.appendChild(textNode);
		// création du passage à la ligne et ajout après le texte
		if (i != nbLignes-1)
		{
			let br = document.createElement("br");
			p.appendChild(br);
		}
		// ajout du paragraphe dans le div
		div.appendChild(p);
	}

	/*
	 * on raccroche le div en fin de document, i.e. on ajoute un fils à "body"
	 * Note : on aurait pu effectuer le raccrochage directement après la
	 *        création du noeud div
	 */
	// getElementsByTagName renvoie un tableau d'éléments : ceux dont le nom
	// de la balise est passé en paramètre.
	// On sait qu'il n'y en qu'un seul "body", c'est pourquoi on récupère
	// directement la case 0
	let body = document.getElementsByTagName("body")[0];
	// appendChild ajoute un fils à la fin de la liste de fils
	body.appendChild(div);
}

