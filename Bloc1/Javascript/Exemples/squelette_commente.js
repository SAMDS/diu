/*
 * Il est préférable de mettre le moins possible de code JavaScript dans
 * les attributs "onqqch" dans le fichier html (et même il ne faudrait
 * aucun code JavaScript).
 * Généralement, dans ces attributs, on met uniquement un appel
 * de fonction (avec éventuellement un paramètre).
 *
 * Lorsqu'on écrit du JavaScript, il est utile d'avoir la console
 * de débogage.
 * Sous firefox :
 *   - soit : Ctrl+Maj+K ou Option+Maj+K (sous macOS)
 *   - soit par le menu hamburger : "Développement web"/"Console web"
 */

// Fonction sans paramètre, branchée sur l'événement 'onload' de la page
function bienvenue()
{
	window.alert('Bienvenue !');
}
