# Bloc1 : exemples commentés javascript

Les fichiers "squelette.xhtml", squelette.css et "squelette.js" proposent
une base pour développer en JavaScript (et CSS) :

- inclusion d'un fichier .css
- un exemple basique de CSS qui affiche les titres principaux en rouge
- inclusion d'un fichier .js dans le .xhtml
- un exemple basique de JavaScript qui affiche un message lors du
  chargement de la page

Les fichiers sont encodés en UTF-8.
L'éditeur de texte doit être paramétré en UTF-8 (sans BOM s'il y a le choix)

Il y a deux versions de chaque fichier. Il y a exactement le même
code dans les deux versions, mais une contient des commentaires et
l'autre non.

En règle générale il est recommandé d'avoir un (ou plusieurs) répertoire(s)
dédié(s) pour :

- les images
- les fichiers css
- les codes JavaScript
- les fichiers html

Par exemple :

SITE
  |
  |--- CSS
  |    |
  |    |--- accueil.css
  |    |--- formulaire.css
  |    \--- principal.css
  |
  |--- IMAGES
  |    |
  |    |--- bandeau.jpg
  |    \--- soleil.png
  |
  |--- JS
  |    |
  |    |--- animation.js
  |    \--- formulaire.js
  |
  |--- PAGES
  |    |
  |    |--- CUISINE
  |    |    |
  |    |    |--- index.html
  |    |    |--- crepes.html
  |    |    \--- lasagnes.html
  |    |
  |    |--- MUSIQUE
  |    |    |
  |    |    |--- index.html
  |    |    |--- classique.html
  |    |    \--- jazz.html
  |    |
  |    \--- index.html
  |
  \--- index.html
