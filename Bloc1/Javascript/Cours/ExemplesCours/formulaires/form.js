var label_date = new Array();
var input_date = new Array();

var label_nom = new Array();
var input_nom = new Array();

function developpeFormation(){
    var fs_formation = document.getElementById("fs_formation");

    var p = document.createElement("p");
    
    label_date[label_date.length] = document.createElement("label");
    label_date[label_date.length-1].appendChild(document.createTextNode("Période"));

    input_date[input_date.length] = document.createElement("input");
    input_date[input_date.length-1].type="text";
    input_date[input_date.length-1].name="dateformation "+ input_date.length;

    label_nom[label_nom.length] = document.createElement("label");
    label_nom[label_nom.length-1].appendChild(document.createTextNode("Nom de la formation"));
    
    input_nom[input_nom.length] = document.createElement("input"); 
    input_nom[input_nom.length-1].type="text";
    input_nom[input_nom.length-1].name="nomformation "+ input_nom.length;

    p.appendChild(label_date[label_date.length-1]);
    p.appendChild(input_date[input_date.length-1]);
    p.appendChild(document.createElement("br"));
    p.appendChild(label_nom[label_nom.length-1]);
    p.appendChild(input_nom[input_nom.length-1]);
    
    fs_formation.appendChild(p);
    fs_formation.appendChild(document.createElement("hr"));
}


var label_date_exp = new Array();
var input_date_exp = new Array();

var label_nom_exp = new Array();
var input_nom_exp = new Array();

function developpeExperience(){
    var fs_experience = document.getElementById("fs_experience");

    var p = document.createElement("p");
    
    label_date_exp[label_date_exp.length] = document.createElement("label");
    label_date_exp[label_date_exp.length-1].appendChild(document.createTextNode("Période"));

    input_date_exp[input_date_exp.length] = document.createElement("input");
    input_date_exp[input_date_exp.length-1].type="text";
    input_date_exp[input_date_exp.length-1].name="dateexperience "+ input_date_exp.length;

    label_nom_exp[label_nom_exp.length] = document.createElement("label");
    label_nom_exp[label_nom_exp.length-1].appendChild(document.createTextNode("Nom de l'entreprise"));
    
    input_nom_exp[input_nom_exp.length] = document.createElement("input"); 
    input_nom_exp[input_nom_exp.length-1].type="text";
    input_nom_exp[input_nom_exp.length-1].name="nomexperience "+ input_nom_exp.length;

    p.appendChild(label_date_exp[label_date_exp.length-1]);
    p.appendChild(input_date_exp[input_date_exp.length-1]);
    p.appendChild(document.createElement("br"));
    p.appendChild(label_nom_exp[label_nom_exp.length-1]);
    p.appendChild(input_nom_exp[input_nom_exp.length-1]);
    
    fs_experience.appendChild(p);
    fs_experience.appendChild(document.createElement("hr"));
}



var input_nom_loisir = new Array();

function developpeLoisirs(){
    var fs_loisirs = document.getElementById("fs_loisirs");

    var p = document.createElement("p");
    
    var lab = document.createElement("label");
    lab.appendChild(document.createTextNode("Loisir " + (input_nom_loisir.length+1)));

    
    input_nom_loisir[input_nom_loisir.length] = document.createElement("textarea"); 
    input_nom_loisir[input_nom_loisir.length-1].name="nomloisir "+ (input_nom_loisir.length+1);
    input_nom_loisir[input_nom_loisir.length-1].rows="4";
    input_nom_loisir[input_nom_loisir.length-1].cols="25";
    input_nom_loisir[input_nom_loisir.length-1].appendChild(document.createTextNode("décris ici un de tes loisirs"));

    p.appendChild(lab);
    p.appendChild(input_nom_loisir[input_nom_loisir.length-1]);
    
    fs_loisirs.appendChild(p);
    fs_loisirs.appendChild(document.createElement("hr"));
}

function check(){
    if (input_nom_exp.length == 0 && input_nom.length == 0){
	window.alert("vous devez entrer au moins une formation ou une expérience professionnelle");
    }
    else{
	document.getElementsByTagName("form")[0].submit();
    }
}

