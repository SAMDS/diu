var movable=false;

function switchMovable(){
    movable=!movable;
}
function move(event){
    if (movable){
	var x = event.pageX;
	var y = event.pageY;
	var img = document.getElementById("chat");

	img.style.position = "absolute";
	img.style.left = x+"px";
	img.style.top = y+"px";
    }
}

function init(){
    document.onmousemove=move;
    document.onclick=switchMovable;
}

