/*
Affiche dans une division à la fin du document le texte contenu dans le
 premier élément fils de l'élément o  
 */
function affiche(o){
    let div = document.createElement("div");
    let texte = o.firstElementChild.firstChild.nodeValue;
    div.append(document.createTextNode(texte));
    document.body.append(div);
}

/*
 Ajout d'écouteur de clics sur tous les li
 */
function todoonload(){
    let li = document.getElementsByTagName("li");
    for (let i = 0 ; i < li.length ; i++){
	    li[i].onclick = function(){affiche(this);};
    }
}
