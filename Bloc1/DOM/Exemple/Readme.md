Bloc 1 - Partie Web
====================================================================

DIU Enseigner l'informatique au lycée

Univ. Poitiers, Canopé, AEFE

## De l'importance d'avoir des pages bien construites.

Vous trouverez ici deux versions de la même page, une incorrecte du point de vue XML (des balises ne sont pas fermées) et l'autre correcte. 

Ces deux pages sont associées à du code javascript et à du style CSS qui s'appuient sur la structure de la page (le DOM).

Il est intéressant de regarder via l'inspecteur de votre navigateur comment ce dernier se "débrouille" avec la page mal construite et quel est l'arbre du document.  


