<?php
	ini_set("display_errors", "on");
	ini_set("expose_php", "on");
	ini_set("error_reporting", E_ALL);

	function my_print_r(&$v)
	{
		echo '<pre>';
		print_r($v);
		echo '</pre>';
	}

	function connexion()
	{
		$bdd = false;
		try
		{
		   $login = 'root';
		   $passwd = '';
		   $bdd = new PDO('mysql:host=localhost;dbname=banque;charset=utf8', $login, $passwd);
		}
		catch (PDOException $e)
		{
		   echo "Erreur : " . $e->getMessage() . "\n";
		   my_print_rprint_r($e);
		   exit();
		}
		return $bdd;
	}

	function requete(&$bdd, $sql, $trace = 'non')
	{
		if ($trace === 'oui')
			echo '[trace] requête : ' . $sql . '<br />';

		$result = $bdd->query($sql);
		
		if ($result && $trace === 'oui')
		{
			echo '[trace] nombre de lignes   : ' . $result->rowCount() . '<br />';
			echo '[trace] nombre de colonnes : ' . $result->columnCount() . '<br />';
		}

		return $result;
	}

	function requete_tab(&$bdd, $sql, $trace = 'non')
	{
		$result = requete($bdd, $sql, $trace);
		$tab = $result->fetchAll(PDO::FETCH_ASSOC);
		$result->closeCursor();
		return $tab;
	}

	function deconnexion(&$bdd)
	{
		$bdd = null;
	}
?>

