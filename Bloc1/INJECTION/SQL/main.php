<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >

	<head>
		<title>injection</title>
		<meta http-equiv="Content-Type"
		      content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" media="all" title="style" href="./main.css" />
	</head>

	<body>

		<h1>Injection HTML et JavaScript</h1>

		<h2>Site d'administration du site de ventes (partie démonstration)</h1>

		<!-- choisissez une des deux lignes -->
		<form action="affiche.php" method="post">
		<!--<form action="affiche_protege.php" method="post">-->
			<p>
				<input type="text" name="login" /> : login (champ à pirater)<br />
				<input type="text" name="passwd" /> : passwd<br />
				<input type="submit" />
			</p>
		</form>

		<h2>Explications</h2>

		<p>Seul le champ <code>login</code> nous intéresse ici. Le
		   but est d'entrer du texte a priori non attendu, et d'analyser
		   la réponse du serveur.<br />
		   Il faut un accès à un serveur de base de données pour faire
		   fonctionner le site.
		</p>
		<div class="cache">
			<p class="cache_visible"><span>Passez la souris
			   sur ce paragraphe pour faire apparaître le texte.</span>
			</p>
			<p>
			   Essayez les entrées suivantes dans le champ <code>login</code>
			   (sans les guillemets).
			</p>
			<ul>
				<li>un login et un mot de passe erronés</li>
				<li>un login et un mot de passe corrects</li>
				<li>"<span class="intrusion">'</span>"&nbsp;: i.e. juste l'apostrophe</li>
				<li>"<span class="intrusion">' OR 1=1 -- </span>"&nbsp;: de l'apostrophe à l'espace final compris</li>
			</ul>
			<p>
				Pour (dé-)sécuriser le site, modifiez le fichier .html aux lignes 20 à 22.
			</p>
		</div>
	</body>
</html>
