<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >

	<head>
		<title>injection</title>
		<meta http-equiv="Content-Type"
		      content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" media="all" title="style" href="./main_demo.css" />
	</head>

	<body>

		<h1>Injection HTML et JavaScript</h1>

		<h2>Site d'administration du site de ventes (partie démonstration)</h1>

		<!-- choisissez une des deux lignes -->
		<form action="affiche_demo.php" method="post">
		<!--<form action="affiche_demo_protege.php" method="post">-->
			<p>
				login <input type="text" name="login" /> &lt;-- champ à pirater<br />
				passwd <input type="text" name="passwd" /><br />
				<input type="submit" />
			</p>
		</form>

		<h2>Explications</h2>
		
		<p>Seul le champ <code>login</code> nous intéresse ici. Le
		   but est d'entrer du texte a priori non attendu, et d'analyser
		   la réponse du serveur.
		</p>
		<div class="cache">
			<p class="cache_visible"><span>Passez la souris
			   sur ce paragraphe pour faire apparaître le texte.</span>
			</p>
			<p>
			   Essayez les entrées suivantes dans le champ <code>login</code>
			   (sans les guillemets).
			</p>
			<ul>
				<li>"<span class="intrusion">moi</span>"&nbsp;: entrée normale sans conséquence</li>
				<li>"<span class="intrusion">&lt;h1&gt;moi&lt;/h1&gt;</span>"&nbsp;: injection HTML</li>
				<li>"<span class="intrusion">&lt;h1&gt;moi</span>"&nbsp;: injection HTML</li>
				<li>"<span class="intrusion">&lt;input type="checkbox" /&gt;moi</span>"&nbsp;: injection HTML</li>
				<li>"<span class="intrusion">&lt;script&gt;alert("moi");&lt;/script&gt;</span>"&nbsp;: injection JavaScript</li>
			</ul>
			<p>
				Pour (dé-)sécuriser le site, modifiez le fichier .html aux lignes 20 à 22.
			</p>
		</div>
	</body>
</html>
