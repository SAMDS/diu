ps processus fs filesystem sh shell
===================================

title: Système d'exploitation  
subtitle: processus, système de fichiers, shell  
author: Philippe Marquet, Univ. Lille  
date: octobre 2019

Distribué sous licence
[CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/),
Attribution 4.0 International. 

Support de présentation
-----------------------

* [Version pour présentation](psfssh-slide.pdf)
* [Version 4 pages par page pour impression](psfssh-4up.pdf)

Contenu du répertoire
---------------------

* [psfssh.md](psfssh.md) - fichier source Markdown 
* [Makefile](Makefile) - as you kwnow
* [fig/](fig/) - les figures - les .svg et .fig sont les fichiers sources 
* [build/](build/) - les .pdf produits 

Sources images
--------------

Sous [Pixabay License](https://pixabay.com/fr/service/license/), Libre pour usage commercial, Pas d'attribution requise

* clavier https://pixabay.com/fr/vectors/clavier-ordinateur-simple-pc-1293389/
* écran https://pixabay.com/fr/vectors/ordinateur-affichage-surveiller-1293303/


