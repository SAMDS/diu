[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/) *Équipe pédagogique DIU EIL AEFE*

Initiation à UNIX, à l'interpréteur de commandes
================================================

Découvrir l'interpréteur de commandes UNIX et les "_commandes de base
  en ligne de commande_", est une capacité attendue du
  [programme de 1re NSI](http://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138157). 

Nous travaillerons ici sous GNU/Linux, en accord avec le fait que "_Les élèves
utilisent un système d’exploitation libre_".

Pour interagir avec le système, nous n'utiliserons donc pas une interface graphique évoluée mais un terminal dans lequel nous taperons les commandes qui nous permettront de dialoguer avec le système.

### Comment travailler dans un environnement GNU/Linux ?

De manière générale, vous avez plusieurs possibilités :

* Installer un système LINUX sur votre machine personnelle, soit comme unique système d'exploitation, soit en double boot
    *Il existe pléthore de distributions LINUX   
    → [Ubuntu](http://ubuntu-fr.org/),   
    → [Debian](https://www.debian.org/)  
    → etc.*  
* Installer sur votre système (Windows, ou MacOS) une machine virtuelle dans laquelle installer LINUX  
    → [Virtual Box](https://www.virtualbox.org/) permet d'installer ce type de machine virtuelle  
    → [Ubuntu 18.04](http://releases.ubuntu.com/18.04/) donne accès à l'image iso d'une distribution Linux parmi d'autres   
* Utiliser une clé (ou un CD/DVD) bootable avec un système linux installé dessus (comme la clé fournie par Canopé)  
    *Attention, il n'est pas possible d'enregistrer quoi que ce soit sur un tel support*

### Organisation du bloc 3.1, du document

Le document présent ainsi que l'organisation ne sont pas figés et sont susceptibles d'évoluer. 

1. Nous commencerons par nous connecter sur une machine distante tournant sous un système Linux  
     → [Connexion à distance](#connexion-à-distance)  
2. Nous exécuterons ensuite pas à pas une série de commandes et en
   observerons le résultat  
   → [Découverte de l'interpréteur de commande](#découverte-de-linterpréteur-de-commande)  
3. Nous utiliserons l'interpréteur de commandes et ferons quelques
   exercices   
   + [Pratique de l'interpréteur de commandes](#utiliser-linterpréteur-de-commandes)
   + [Système de fichiers](#bases-du-système-de-fichiers)
   + [Gestion des droits](#droits)
   + [Processus et entrées-sorties](#processus-et-entrées-sorties)
   + [Variables d'environnement et substitutions](#variables-denvironnement-et-substitutions)
   + [Les scripts](#les-scripts) 

Pour vous aider, une très brève introduction à l'interpréteur de commandes UNIX vous est fournie dans le fichier [shell-bref.md](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc3/seance0/shell-bref.md).  Elle peut permettre de découvrir UNIX, le shell - interpréteur de commandes -, et quelques-unes des commandes de base. 
On pourra s'y référer par la suite.

Connexion à distance
====================

Au-delà de l'interaction avec un système via la ligne de commande, les objectifs ici sont aussi de :

* découvrir comment travailler sur une machine distante
* interagir avec un système multi-utilisateurs

Pour se connecter à distance, nous allons utiliser le protocole SSH. 

* Si vous êtes déjà sous Linux ou MacOS, ou que vous disposez de PowerShell sous Windows, commencez par ouvrir un terminal (Pour MacOS, il est dans le répertoire /Applications/Utilitaires/)
* Si vous êtes sous Windows sans PowerShell, le plus simple est d'installer un utilitaire :  
    → [MobaXterm](https://mobaxterm.mobatek.net/) (*mon préféré*)   
    → [Putty](https://putty.org/)  

Nous allons nous connecter sur une machine hébergée par l'Université.
Son adresse IP publique est : **195.220.223.226**
et le port sur lequel se connecter est le port **86** 
Elle a aussi un petit nom : **voca2.univ-poitiers.fr** 

*Nous vous avons créé des comptes utilisateur sur cette machine. Ils seront ouverts pendant la durée de la formation mais fermés ensuite. Cependant lorsque les inscriptions à l'Université seront effectives, vous aurez accès à une autre machine à distance avec vos comptes étudiants.* 

*N.B. : Tout ce qui concerne plus spécifiquement les aspects réseaux vous sera présenté dans la partie 3.5*

* Si vous êtes sous Linx ou MacOS, il suffit de saisir dans votre terminal puis de valider avec le touche `entrée` la commande :
  `ssh -p 86 -l votrelogin 195.220.223.226` ou `ssh -p 86 votrelogin@195.220.223.226`

* Si vous travaillez sous Windows avec MobaXterm, il vous faut :
  
  1. aller dans `Sessions->New session`, 
  2. sélectionner `ssh`, 
  3. dans `Basic SSH Settings` 
     1. indiquer l'adresse IP dans Remote host
     2. cocher "specify username" et ajouter votre login
     3. choisir le port 86
  4. valider avec `Ok`
  
  *Au passage, on peut remarquer que ce qui se fait en une ligne dans un terminal demande quelques manipulations dans une interface graphique*
  
  Dans les deux cas, votre mot de passe vous est demandé, saisissez-le. Vous devez maintenant être connecté. 
  
  *N.B. : Lors de la première connexion, il vous est demandé de changer le mot de passe. Saisissez deux fois votre nouveau mot de passe. Vous êtes alors déconnecté et il vous reste à vous reconnecter avec votre nouveau mot de passe.*

Découverte de l'interpréteur de commandes
=========================================

Un terminal pour nos premiers pas
---------------------------------

Vous êtes maintenant connecté sur la machine distante par l'intermédiaire d'un terminal.
La machine distante ne possède aucun environnement graphique, les manipulations proposées ici seront donc forcément réalisées via l'interpréteur de
commandes, directement dans le terminal.

Premières commandes
---------------------

Le terminal ouvert sur la machine distante attend que vous lui donniez vos premières instructions. 

Il signale qu'il attend que vous tapiez une commande en utilisant une
"invite" (prompt en anglais), c'est à dire un message d'attente. Par exemple, en ce qui me concerne : `alayrang@voca2:~$`

**Quelle est la vôtre ?**

### Interpréteur de commandes

Plusieurs interpréteurs de commande (`shell` en anglais) peuvent être utilisés pour interagir avec le système Linux. On interagit avec un interpréteur en tapant des commandes et en appuyant sur la touche `ENTRÉE` pour que la commande soit interprétée et donc exécutée.

Pour savoir quel interpréteur de commandes est utilisé ici, tapez la commande ci-dessous qui demande l'affichage de la variable d'environnement dans laquelle est mémorisé le shell utilisé : `echo $SHELL`

`SHELL` fait, en effet, partie des *variables d'environnement* qui permettent
de définir un certain nombre d'informations utiles, notamment l'environnement d'exécution des programmes.

**Quel est l'interpréteur de commandes utilisé ?**

Plusieurs interpréteurs peuvent être utilisés. La liste des interpréteurs utilisables sur un système Linux donné se trouve dans le fichier `/etc/shells`. Vous pouvez visualiser le contenu de ce fichier (et
d'autres) grâce à la commande *less*.

**Quels sont les interpréteurs utilisables sur la machine distante ?**

```console
less /etc/shells
```

La commande *less* affiche le contenu d'un texte "page par page" : pour avancer d'une page, appuyez sur la barre d'espace, vous pouvez également naviguer dans le texte avec les flèches haut et bas. Pour quitter, tapez *q*.

À partir de là, sauf mention explicite du contraire, on utilisera l'interpréteur */bin/bash*.

> Notez au passage que l'interpréteur de commandes vous offre quelques facilités pour la saisie :
> il est possible de naviguer dans l'historique des commandes exécutées dans le shell via les flèches haut et bas. Votre historique n'est pas encore bien fourni mais vous pouvez néanmoins tester.
> 
> * lorsque vous tapez un début de commande et appuyez deux fois sur la touche tabulation, le shell vous propose l'ensemble des noms de commande ainsi.
>   Par exemple, tapez "le" dans le shell puis enfoncez deux fois la touche
>   tabulation. Notez l'ensemble des commandes qui commencent par "le".
> 
> * lorsque vous avez tapé une ligne, et avant de la valider, vous pouvez naviguer dans celle-ci avec des combinaisons de touches (la touche contrôle simultanément avec une touche caractère) :
>   
>   * CTRL+A : amène au début de la ligne
>   * CTRL+E : amène à la fin de la ligne
>   * CTRL+B : recule d'un caractère sur la ligne
>   * CTRL+F : avance d'un caractère sur la ligne

### Quelle version du système est-elle installée ?

Pour avoir des informations sur la distribution de Linux installée sur une machine il suffit de regarder le fichier `/proc/version`.
Pour regarder le contenu d'un fichier texte, on peut utiliser la commande `less` dans le terminal :

```console
    less /proc/version
```

La commande `less` affiche le contenu d'un texte "page par page" : pour avancer d'une page, appuyez sur la barre d'espace, vous pouvez également naviguer dans le texte avec les flèches haut et bas. Pour quitter, tapez `q`.

**Quelle distribution est-elle installée la machine distante ?**

Exécutions pas à pas
--------------------

> Le fonctionnement d'un interpréteur de commandes se décompose en plusieurs étapes :
> 
> 1. **phase de saisie** : correspond à la lecture d'une ligne de commandes, et peut mettre en jeu plusieurs mécanismes facilitant la saisie (complétion, historique, édition de la ligne de commandes...)
> 2. **phase de substitution** : consiste en l'analyse de la ligne de commandes et l'application de diverses substitutions (par exemple,  *\*.tex* désigne l'ensemble des fichiers du répertoire courant dont le nom se termine par *.tex*)
> 3. **phase d'exécution** : lance la recherche et l'exécution de la commande résultant de la phase précédente.
>    De plus, tout interpréteur de commandes, dispose d'un **environnement** qui définit le cadre dans lequel il travaille ainsi que son propre comportement. La variable d'environnement *PATH* indique ainsi par exemple où aller chercher les exécutables. 
>    Il dispose également de commandes internes qui peuvent varier d'un interpréteur à l'autre.
>    Enfin, un shell n'est pas seulement un interprète de commandes, il
>    fournit aussi un langage de programmation de scripts, avec possibilité
>    d'utiliser des variables utilisateurs, des structures de contrôles,...

Les commandes qui suivent sont *à saisir dans le terminal*. 
Validez ensuite en appuyant sur Entrée et observez l'action effectuée.

Lorsque vous entrez des commandes dans un terminal, celles-ci s'exécutent dans
le répertoire courant, par défaut la racine de votre répertoire personnel.

1. Le contenu d'un répertoire peut être obtenu au moyen de la commande
   `ls`. 
   Saisissez cette commande, le terminal affiche les fichiers présents dans le 
   répertoire courant.

2. Nous allons maintenant créer un nouveau répertoire : utilisez la commande `mkdir` 
   (make directory) suivie du nom du répertoire. Ici `mkdir
   bloc3`. Observez le résultat par une nouvelle exécution de `ls`. 

3. Déplaçons nous dans le répertoire bloc3 nouvellement créé : `cd bloc3` (change 
   directory). Constatez que l'invite de l'interpréteur a été modifiée, indiquant
   que nous sommes maintenant dans le répertoire `bloc3`.

4. Utilisez la commande `pwd` pour afficher le répertoire courant.

5. Pour les besoins du TP, 
   
   * créons un nouveau répertoire : `mkdir tim`
   * plaçons nous dans ce répertoire : `cd tim`
   * affichons le répertoire courant : `pwd`

6. Nous allons créer un fichier dans le répertoire `tim` :
   
   * La commande `touch oleon` crée un fichier (vide) appelé `oleon`,
   * Vérifions à l'aide de la commande `ls` que le fichier a bien été créé.

7. Remontons dans l'arborescence : 
   
   * `cd ..` 
   * Vérifions que le répertoire courant a bien été modifié : `pwd`
   * Créons un deuxième répertoire : `mkdir raymond`

8. Pour *copier un fichier* :
   
   * on utilise la commande `cp` : `cp tim/oleon raymond/`
   * Cette commande a pour effet de copier le fichier `oleon` dans le 
     répertoire `raymond/`
   * Constatez son effet : `ls raymond/` qui affiche le contenu du répertoire
     `raymond`

9. Nous allons maintenant tenter de supprimer le répertoire `tim` : 
   
   * `rmdir tim` (remove directory) vous constatez que la commande produit une erreur
     puisque le répertoire n'est pas vide.
   * Il est possible de supprimer un répertoire non vide, mais pour 
     l'instant allons effacer le fichier `oleon` qui n'est de toute façon 
     pas d'une grande utilité : `cd tim` puis `rm oleon` (remove).

10. Tentons à nouveau la suppression du répertoire : 
    
    * `cd ..` 
    
    * `rmdir tim`
      
      Vous constatez que la commande ne signale plus d'erreur et que le 
      répertoire a été supprimé.

11. Nous pouvons également déplacer des fichiers : 
    
    * `mv raymond/oleon .`
    
    Cette commande déplace (move) le fichier `oleon` de `raymond` dans 
    le répertoire courant `./` (ici `bloc3`).

12. La même commande `mv` permet aussi de renommer un fichier. Essayez
    `mv oleon arthur`. Observez. 
    
    *N.B. : si l'on considère que le nom d'un fichier est constitué du chemin qui mène jusqu'à lui dans l'arborescence de fichiers, alors "déplacer" et "renommer" un fichier sont une seule et même action.*

13. Le répertoire `raymond` ne contient plus rien : 
    
    * `ls raymond/` 
    * Nous pouvons le supprimer : `rmdir raymond`

14. Saisissons les commandes suivantes :
    
    * `cp arthur Arthur`
    * `ls`
    
    Observez que le répertoire contient maintenant deux fichiers *différents*
    dont les noms sont `arthur` et `Arthur` : Le système de fichier est donc
    *sensible à la casse*.

15. Saisissons :
    
    * `cp arthur .arthur`
    * `ls`
    
    Observez que le contenu du répertoire ne semble pas avoir été modifié. Pourtant 
    le fichier `.arthur` a bien été créé :
    
    * `ls .arthur`
    
    Il s'agit d'un *fichier caché*. 
    La convention adoptée et que tous les fichiers dont le nom commence 
    par un point `.` sont traités comme des fichiers cachés.

16. Pour visualiser *tous* les fichiers d'un répertoire (y compris les cachés) :
    
    * `ls -a`
    * ou `ls -al` 
    
    Observez la différence entre les deux commandes. 
    Testez `ls` avec l'option `-l` seule. En déduire l'effet de l'option `-l`. 

17. Observons que notre répertoire contient une entrée nommée `.` qui représente 
    le répertoire courant et une entrée nommée `..` qui correspond au répertoire
    supérieur. On retrouve ces deux entrées dans tous les répertoires.

18. Nettoyons maintenant notre répertoire :
    
    * `rm arthur Arthur .arthur` (la commande `rm` accepte plusieurs arguments)
    * `cd ..`
    * `rmdir bloc3`

> Le disque contient une grande quantité d'informations rangées dans des répertoires ou dossiers et des fichiers : pour le système, pour les applications, pour chaque utilisateur,...
> 
> Les fichiers sont répartis dans des répertoires (ou dossiers) qui eux-mêmes peuvent contenir des sous-répertoires et ainsi de suite. Cette organisation constitue une arborescence.
> 
> Tous les fichiers sont identifiables par leur nom et leur emplacement dans la hiérarchie. On doit pour cela fournir la liste des répertoires qu’il faut traverser avant de parvenir dans le répertoire contenant le fichier. On appelle cette information le chemin (*path* en anglais) du fichier. Chaque répertoire d’un chemin est séparé du répertoire suivant par le caractère «/».
> 
> Dans cette arborescence, vous êtes toujours situé dans un répertoire précis (appelé *répertoire courant*) que l'on peut changer à volonté. L'intérêt de cette notion est de pouvoir accéder rapidement aux fichiers (situés dans le répertoire courant où à proximité) sans préciser systématiquement où ils se trouvent exactement.
> 
> Il y a donc deux manières pour désigner un fichier :
> 
> * **chemin absolu** : on indique la suite de répertoires, *en partant du répertoire racine*, qu'il faut parcourir avant d'atteindre le fichier visé. Cette notation est non ambiguë mais est lourde à gérer. Un chemin absolu commence par le signe ``/'' qui désigne le répertoire principal ou racine.  C'est le principe d'une adresse postale complète : */France/86000Poitiers/RueSainteCatherine/6*
> * **chemin relatif** : on indique la suite de répertoires,  *en partant du répertoire courant* qu'il faut parcourir avant d'atteindre le fichier.  La signification de ce chemin dépend du répertoire courant. Un chemin relatif ne commence pas par le signe ``/''. C'est l'équivalent de renseigner un passant, par exemple, lorsqu'on est à l'espace Mendès France à Poitiers et que l'on veut aller à Canopé Poitiers : *2ème à gauche/au bout de la rue à droite/première à gauche*
> 
> L'interpréteur de commandes permet de désigner le répertoire personnel à l'aide du caractère *~*. On écrit par exemple :
> 
> ```console
>  ~/Documents/info.txt
> ```
> 
> pour désigner le fichier *info.txt* situé dans le répertoire *Documents* de l'utilisateur connecté.

Utiliser l'interpréteur de commandes
====================================

## Bases du système de fichiers

1. Affichez le répertoire courant (celui où vous vous trouvez).
2. Affichez (sans vous déplacer) le contenu du répertoire racine.
3. Déplacez vous dans le répertoire `etc` situé à la racine du système de fichiers en utilisant un nom absolu.
4. Déplacez vous dans votre répertoire de connexion en utilisant un
   nom relatif.

## Le manuel est votre ami

Les commandes sont documentées dans un manuel, accessible en ligne de commande via la commande :

```console
man nom_de_la_commande
```

Il faut connaître le nom de la commande pour accéder à son manuel. 

En vous aidant du manuel, répondez aux questions suivantes : 

1. Listez le contenu du répertoire */home/DIU* dans l'ordre alphabétique inverse

2. Listez le contenu du répertoire */home/DIU* par ordre de modification des fichiers : de la modification la plus récente à la plus ancienne

3. Quelle est l'option de la commande *mv* qui avertit l'utilisateur lorsque le nom de destination est un fichier existant (ce sera la même chose pour la commande *cp*). Notez que selon les configurations, cette option n'est pas toujours activée par défaut.

4. La commande *du* permet de déterminer la place occupée par un fichier / un dossier. Sans argument, elle s'applique au répertoire courant. Trouvez la taille du répertoire courant et du répertoire */lib/*. Quelle option permet-elle d'obtenir la taille de tous les fichiers contenus dans l'arborescence qui commence au répertoire courant ?
    Parfois les tailles ne sont pas lisibles car trop grandes. Quelle option permet-elle un affichage avec des unités de capacité (Ko, Mo, Go) adéquates ? Essayez sur le répertoire */lib*.
    Éventuellement l'affichage défile alors que seul le résultat global nous intéresse. Quelle option permet de n'avoir que le résultat final ?
   
   Est-ce que le manuel contient des informations sur toutes les commandes que l'on peut utiliser ?
   Essayez de regarder le manuel de la commande *cd*... Pas d'entrée pour cette commande pourtant essentielle. Pourquoi ? Il s'agit d'une commande directement intégrée à l'interpréteur de commandes, on en trouve la description dans la (longue) page de manuel de *bash*.

## Manipulation du système de fichiers

Il existe plusieurs types de fichiers. Lorsqu'on utilise la commande *ls* avec l'option *-l*, la première lettre de la ligne consacrée à chaque fichier donne une indication sur le type du fichier en question.

 **Quels sont les types des fichiers suivants ?**

*Pour répondre à cette question, vous pourrez vous aider d'une description plus détaillée de la commande `ls` accessible via  `info ls`  

N.B. : la commande `info`, lorsqu'elle est installée, fournit des informations sur la commande sur laquelle elle est appelée. A minima, elle présente la page de manuel de la commande mais souvent elle contient des informations supplémentaires. Les informations sont généralement décomposées en plusieurs sections, il suffit de déplacer le curseur avec les flèches sur le titre de la section et d'appuyer sur la touche entrée pour la visualiser*

* *~/.bashrc*

* fichiers contenus dans */home/DIU/*

* */dev/stdout*

* */dev/tty0* 

* */dev/sr0*
  
  Pour disposer d'informations plus précises sur un fichier on peut aussi utiliser la commande *file* suivie du nom du fichier. Testez sur les fichiers précédents.

> Sous Linux, les dossiers mais aussi les périphériques d'entrée/sortie (écran, clavier,...), les périphérique de stockage (disques dur, cdrom, dvd, clés USB, etc.) sont "vus" comme des fichiers.    
> Les informations sur les fichiers sont gérées par le système sous forme de [nœud d'index ou *inode*](https://fr.wikipedia.org/wiki/N%C5%93ud_d%27index). Chaque inode est identifié par un numéro. Un répertoire est en fait un fichier qui contient une liste de numéros d'inodes associés à des noms (les noms de fichiers ou de répertoires...).
> Un lien permet de lier un nom de fichier et un inode. Il existe plusieurs types de liens. Les liens physiques qui créent un nouveau nom pour un inode déjà existant et les liens symboliques (ou raccourcis) qui créent un nouvel inode de type lien
> symbolique (associé à un nouveau nom), cet inode contenant "juste" le chemin vers le fichier réel.
> Les liens symboliques sont l'équivalent des raccourcis sous d'autres systèmes d'exploitation
> 
> Pour créer un lien (que ce soit physiques ou symboliques), on utilise la commande `ln`.

1. Créez un répertoire `systeme` et placez vous dedans.
2. Copiez le répertoire `fichiers_diu/fichiers` situé à la racine du compte `alayrang` dans votre répertoire `systeme`.
3. Placez vous dans le répertoire `a_faire`. Donnez le chemin absolu du fichier `gestion` et les chemins relatifs des fichiers `algo` et `anglais`. Vérifiez qu’ils sont corrects en utilisant la commande `cat` pour visualiser leurs contenus en utilisant ces chemins.
4. Réalisez la suite d’opérations demandées ci-après. Pour toutes ces
   commandes, votre répertoire de travail doit rester `a_faire`. Vous
   pouvez utiliser la commande `ls -i` (ou `tree --inodes`) pour
   vérifier l'effet de ces commandes en terme d'inodes.
   + recopiez le contenu du fichier `algo2` dans le fichier `algorithmique`
   + recopiez le fichier `algo2` dans le fichier `algo` du répertoire `fait`
   + renommez le fichier `anglais` en `english`
   + déplacez le fichier `english` dans le répertoire `fait`
   + faites en sorte que le fichier `math` s’appelle également `abandon`
     dans le répertoire `fait` (i.e. on créer un lien physique)
   + éditez le fichier `abandon`, par exemple, avec *nano* et y mettre le mot `regret`.
   + visualisez le contenu du fichier `math`.
   + avec la commande `ls -l`, regardez le nombre de liens physiques du
     fichier `math`. A quoi correspondent-ils ?
   + créez un lien symbolique nommé ̀`persevere` pointant sur le fichier
     `abandon`.
   + avec la commande `ls`, regardez le nombre de liens physiques du
     fichier ̀`math`.
   + visualisez le contenu du fichier `persevere`.
   + supprimez le fichier `abandon`.
   + visualisez le contenu du fichier `persevere`.
5. Déterminez le nombre de liens physiques pour le répertoire
   `a_faire` et à quoi ils correspondent.

## Droits

> Les fichiers et répertoires sont protégés par des "droits" vous interdisant (ou vous autorisant) à y accéder et/ou les consulter et/ou les modifier.
> 
> Le monde est séparé en 3 catégories~:
> 
> * vous (utilisateur ou propriétaire)
> * votre groupe (les participants au diu par exemple)
> * les autres
> 
> Un utilisateur peut appartenir à plusieurs groupes. Mais sous Linux, un fichier est forcément associé à un seul groupe donné.

Pour connaître les groupes auxquels vous appartenez, utilisez la
commande  *groups* (sans argument).
Si vous voulez connaître les groupes auquel j'appartiens~: *groups alayrang*

Essayez les commandes suivantes et observez leur résultat :

* less /etc/passwd
* rm /etc/passwd
* touch /etc/moi
* ls /home/DIU/alayrang/
* ls /home/DIU/alayrang/Bienvenue
* cat /home/DIU/alayrang/Bienvenue/bonjour.txt
* rm /home/DIU/alayrang/Bienvenue/bonjour.txt
* touch /tmp/votrelogin.txt

> Pour connaître les droits associés aux fichiers présents dans un répertoire, on utilise (encore) la commande `ls -l`. 
> 
> Chaque ligne commence par 10 caractères. Le premier sert à indiquer la nature du fichier (répertoire, fichier normal, lien,...). Les 9 suivants concernent les droits des trois catégories à raison de 3 caractères par catégorie.
> 
> Pour une catégorie, le premier caractère  vaut `r` ou `-` indiquant ou non un droit en lecture (*read*). Le deuxième caractère  vaut `w` ou `-` indiquant ou non un droit en écriture (*write*). Le troisième caractère vaut 'x' ou '-' indiquant ou non un droit en exécution (*execute*).
> 
> Par exemple, la chaîne `-rw-r-x--x` signifie (on saute le premier caractère indiquant un fichier normal) :
> 
> * vous avez un droit de lecture, de modification mais pas d'exécution
> * le groupe auquel est lié ce fichier a un droit de lecture, d'exécution mais pas de modification
> * les autres ne peuvent qu'exécuter le fichier.

Reprenez les fichiers manipulés dans la question précédente et regardez les droits associés. 

> Pour modifier les droits sur un fichier, on utilise la commande `chmod`
> Une syntaxe possible que l'on nomme **forme symbolique** est la suivante~: 
> 
> ```console
> chmod [ugoa][-+][rwx] nom_de_fichier_ou_repertoire
> ```
> 
> Avec les significations suivantes :
> 
> * u :  indique que l'on modifie les droits du propriétaire (**u**ser) 
> * g : indique que l'on modifie les droits du **g**roupe 
> * o : indique que l'on modifie les droits des autres (**o**thers) 
> * a = indique que l'on modifie les droits des trois catégories simultanément 
>   N.B. :  il est possible de spécifier une ou plusieurs lettres 
> * \+ : indique que l'on ajoute un droit 
> * \- : indique que l'on supprime un droit 
> * = : indique que l'on donne un droit et que l'on enlève les autres 
>   N.B. : +, - et = sont exclusifs 
> * r : indique que le changement concerne le droit en lecture (**r**ead) 
> * w : indique que le changement concerne le droit en écriture (**w**rite) 
> * x : indique que le changement concerne le droit en e**x**écution 
>   N.B. : il est possible de spécifier plusieurs lettres 
> 
> Par exemple:
> 
> ```console
> chmod go-w ...
> ```
> 
> indique que l'on enlève les droits d'écriture au groupe et aux autres.
> 
> ```console
> chmod u=r
> ```
> 
> est équivalent à  `chmod u+r,u-wx`
> 
> Il existe une autre syntaxe pour changer les droits, on parle de **forme numérique**, vous pouvez trouver les détails sur [Wikipedia](https://fr.wikipedia.org/wiki/Permissions_UNIX#Repr%C3%A9sentation_des_droits)

### Exercices

1. Placez vous à nouveau dans le répertoire `fichiers`.

2. Utilisez la commande `id` pour visualiser votre identifiant
   d'utilisateur (uid), votre groupe principal (gid) ainsi que les
   groupes auxquels vous appartenez.

3. Créez un répertoire `prive` dans lequel vous créerez un fichier
   nommé `prive` contenant votre nom de login et un répertoire `partage`.
   
   + Interdire l’accès au
     répertoire `prive` pour les membres du groupe et les autres.
   + Dans le répertoire `partage` créez un fichier `lecture` dans lequel
     vous mettrez votre nom de login. Ce fichier devra être consultable
     mais non modifiable par les membres de votre groupe principal et
     non lisible/modifiable par les autres. Modifiez les droits. 
   + Dans le répertoire `partage` créez un fichier `ecriture` dans
     lequel vous mettrez votre nom de login. Ce fichier devra être
     consultable et modifiable par les membres de votre groupe
     principal mais pas par les autres. Modifiez les droits.

4. Demander à votre voisin de tester vos droits en :
   
   + Essayant de lire le contenu du fichier `prive`.
   + Essayant de lire puis de modifier le contenu du fichier `lecture`.
   + Ajoutant son nom de login à votre fichier `ecriture`

5. Éditez un fichier nommé `salut` avec le contenu suivant :
   
   ```
   echo Hello World
   ```
   
   Tentez de l'exécuter en tapant `./salut`. Modifiez les droits sous
   **forme symbolique** de manière à ce que vous puissiez l'exécuter
   puis vérifiez que vous pouvez l'exécuter.
   
   *N.B. : vous venez d'écrire votre premier script... En effet, `echo` est une commande interne du shell et le résultat de l'exécution du fichier, est tout simplement le résultat de la commande `echo Hello World`.
   
   Si vous n'aviez pas écrit une (ou plusieurs) commande(s) dans le fichier `salut`, la tentative d'exécution aurait provoqué une erreur de type "commande introuvable" *

## Quelques commandes utiles

Voici quelques commandes fort utiles sur lesquelles on reviendra [ultérieurement](Les filtres).

1. Les commandes `head` et `tail` permettent de récupérer respectivement les premières et dernières lignes d'un fichier. 

En vous aidant du manuel, affichez les 2 premières lignes puis les 3 dernières lignes du fichier `/etc/passwd`

2. La commande `cut` permet de sélectionner des parties de ligne dans un fichier ou sur l'entrée standard. En regardant le fichier `/etc/passwd`, vous avez pu constater que chaque ligne est décomposée en plusieurs champs deux à deux séparés par un ":". 
   En vous aidant du manuel, trouvez comment utiliser `cut` pour récupérer et afficher la valeur du premier, du troisième et du sixième champ de chaque ligne de ce fichier.

3. La commande `wc` permet de compter un certain nombre de choses. Toujours en utilisant le manuel, trouvez comment utiliser cette commande pour compter le nombre de lignes du fichier `/etc/group`.

4. La commande `grep` permet de chercher une expression régulière dans un fichier. Pour savoir ce qu'est une expression régulière, vous pouvez regarder les explications dans le manuel étendu de `grep` accessible via la commande `info grep`
   En vous aidant des manuels accessibles via la commande `man` et via la commande `info` :
   
   1. Cherchez dans le fichier `/etc/group`, la ligne contenant le mot `nogroup`
   
   2. Cherchez toutes les lignes du fichier `/etc/group` qui commencent par la lettre 'c' (pour trouver comment vous y prendre, cherchez le terme `ANCHORING` dans le manuel
   
   3. Cherchez toutes les lignes du fichier `/etc/passwd` qui se terminent par `false`
   
   4. Cherchez les lignes du fichier `/etc/passwd` dont le premier champ ne contient que des caractères alphanumériques

5. La commande `find`, comme son nom l'indique, permet de... trouver des fichiers.
   
   1. Utilisez la commande `find` pour trouver tous les fichiers cachés (autrement dit dont le nom commence par un `.`) situés dans votre répertoire racine et ses descendants 
   
   2. Utilisez la commande `find` pour trouver tous les fichiers avec l'extension `.log` situés dans la hiérarchie de répertoires enracinée sur `/home/etu`. 
   
   3. Utilisez la commande `find`pour trouver tous les fichiers présents sur votre compte que vous avez modifiés dans les dernières 24h 
   
   4. Créez un répertoire caché, appelé `.TRASH` dans votre répertoire racine. Puis à l'aide de la commande `find` (et de l'option `exec`), déplacez dans ce répertoire tous lesfichiers temporaires de votre compte, a minima les fichiers dont le nom se termine par un ~, et ceux dont le nom commence et se termine par un #.
      
      **Attention : dans un premier temps, avant de déplacer les fichiers (ce qui peut causer des soucis si votre commande est mal construite), demandez l'affichage des noms des fichiers trouvés avec la commande `echo`, puis pour éviter les problèmes, utilisez (au moins dans une première version) l'option de la commande `mv` qui demande confirmation à l'utilisateur avant de déplacer les fichiers}.**

## Processus et entrées-sorties

### Commandes et processus

> L'exécution d'une commande par le shell donne lieu à un nouveau processus.
> Par défaut les processus démarrés par le langage de commande le sont
> en **avant-plan** : vous ne pouvez exécuter de nouvelles commandes (on
> dit que vous n’avez pas la main sur le terminal) tant que le processus
> démarré n’est pas terminé.
> Il est possible de démarrer une commande via un processus en
> **arrière-plan** (ou tâche de fond). Pour cela il suffit de faire
> suivre la commande souhaitée par le caractère « & ».
> 
> Les langages de commande permettent de démarrer plusieurs commandes en
> *séquence* via l’utilisation du caractère de séparation « ; » ou en
> *parallèle* via l’utilisation du caractère « & ».
> 
> De plus il est possible de grouper le démarrage des processus d’une
> suite de commande en les entourant de parenthèses. Par exemple, si un
> processus à créer en arrière-plan doit comporter plusieurs commandes
> successives il suffit de grouper les commandes avec des parenthèses,
> de séparer les commandes souhaitées par des points virgule et de
> placer le caractère « & » après la dernière parenthèse fermante comme
> dans l’exemple suivant :
> 
> ```
> ( commande1 ; commande2 ) &
> ```
> 
> Dans ce cas le shell crée un nouveau processus qui lui même va créer, en séquence, 2 autres processus, le premier effectuant le code contenu
> dans `commande1` puis un second effectuant le code contenu dans `commande2`.
> 
> Chaque processus est repéré par un identifiant système unique
> (PID). La commande `ps` permet de lister les processus en cours
> d’exécution sur le système.
> 
> Dans certains shells (dont `bash`) il existe une notion connexe aux
> identifiants de processus : les identifiants de tâches ou de
> **jobs**. Un job est un processus attaché à un terminal en cours
> d’utilisation. Dans les lignes de commandes on représentera les jobs
> par le caractère « % » suivi du numéro identifiant la tâche (par
> ex. `%2`). La commande `jobs` permet de lister les tâches en cours et
> leur état. Pour chaque démarrage en arrière-plan le langage de
> commande signifie à l’utilisateur le numéro de job, suivi du numéro de
> processus (PID).
> 
> En bash si un processus est exécuté en avant plan, il est possible de le suspendre en frappant simultanément sur les touches `Ctrl` et `Z`. Un processus suspendu peut être redémarré en avant-plan via la commande `fg` (pour foreground), ou en arrière plan avec la commande `bg` (pour background).
> 
> La commande `kill` permet d’envoyer des [signaux](https://fr.wikipedia.org/wiki/Signal_(informatique)) aux
> processus. Cette commande demande au moins deux paramètres. Le premier
> permet de préciser le signal à envoyer au processus. Le second
> correspond a l’identification du processus à signaler (via un PID ou
> numéro de job). Trois signaux sont particulièrement utile : 
>     + KILL qui demande la destruction sans condition d’un processus;
>     + STOP qui en demande la suspension
>     + CONT qui demande le redémarrage d’un processus préalablement stoppé.

#### Exercices

Pour ces exercices, il va vous falloir vous connecter sur une machine distante de l'université (ou bien utiliser un Linux en local si vous en avez un).

Si vous n'avez pas encore activé votre compte de services en ligne, rendez-vous d'abord sur https://sel.univ-poitiers.fr pour le faire. Vous aurez ensuite accès à votre nom de connexion (login) de l'Université (souvent la première lettre du prénom et les 7 premières lettres du nom)

La commande à utiliser pour la connexion est :

```bash
ssh -Y -p 86 -l votreloginUniversite cartan.campus.univ-poitiers.fr
```

Si vous ne parvenez pas à vous connecter, passez à la section d'[exercices alternatifs](#Exercices-alternatifs-en-attendant-vos-comptes-sur-Cartan).

*(il peut y avoir un temps de latence entre votre inscription à l'université et la création de votre compte informatique, vous y aurez accès ultérieurement depuis chez vous)*

1. À quoi correspond le paramètre `-Y` de la commande `ssh` ci-dessus ?

2. Démarrez la commande `xeyes`.

3. Essayez maintenant de démarrer une nouvelle fois la commande `xeyes`. Est-ce possible ?

4. Stoppez la commande démarrée en frappant simultanément les touches `Ctrl` et `Z`.

5. Redémarrez la commande stoppée en arrière-plan.

6. Tuez le processus correspondant à `xeyes` en utilisant son numéro de job.

7. Lancez un nouveau processus `xeyes`ainsi qu'un processus `xclock`. Trouvez le numéro des processus de `xeyes` et `xcalc` et utilisez
   les pour les stopper au moyen de la commande `kill`. Vérifiez que
   ces processus sont figés.

8. Faites redémarrer le processus `xeyes` au moyen de la commande
   `kill`.

9. Faites redémarrer le processus `xclock` en avant plan au moyen de
   la commande `fg`

10. La commande `top` permet d'afficher en "temps réel" les processus en cours d'exécution.
     Lancez la commande top dans votre terminal. Appuyez sur la touche `u` puis entrez votre login, appuyez sur la touche ENTRÉE. Vous visualisez maintenant uniquement vos propres processus. Demandez le login de votre voisin et demandez à `top` d'afficher uniquement ses processus.
    
     Appuyez sur `f` puis sélectionnez la colonne PID, appuyez sur la touche `s`, puis `ESC` pour retourner à l'affichage. Voyez-vous ce qui a changé ? Si vous ne voyez pas le résultat, refaites la même opération en sélectionnant une autre colonne.
    
     Changez la granularité temporelle du rafraichissement en tapant `s` puis en entrant la nouvelle durée (1.0 par exemple).

11. Affichez l'arbre des processus tournant sur la machine avec la commande `pstree`(et oui encore et toujours des arbres !)

12. Débarrassez vous de tous ces processus avec la commande `kill`.

### Exercices alternatifs en attendant vos comptes sur Cartan

1. Démarrez la commande `nano` dans un terminal sur la machine distante (`<195.220.223.226`) .

2. Stoppez la commande démarrée en frappant simultanément les touches `Ctrl` et `Z`.

3. Vérifiez que le processus existe toujours en utilisant la commande `ps`avec l'option adéquate pour visualiser vos processus.

4. Redémarrez la commande stoppée en arrière-plan.

5. Tuez le processus correspondant à `nano` en utilisant son numéro de job.

6. Démarrez une nouvelle fois, la commande `nano`.  À partir de votre machine, connectez vous sur la machine distante dans un nouveau terminal. Vous devez donc maintenant avoir deux terminaux ouverts sur la machine distante : un où tourne `nano`, l'autre où tourne uniquement un interpréteur `bash`. Trouvez, en utilisant ce nouveau terminal, le numéro du processus de`nano` et utilisez le pour le stopper au moyen de la commande `kill`. Vérifiez que le processus est bien figé.

7. Faites redémarrer le processus `nano` au moyen de la commande `kill`.

8. La commande `top` permet d'afficher en "temps réel" les processus en cours d'exécution.
    Lancez la commande top dans votre terminal. Appuyez sur la touche `u` puis entrez votre login, appuyez sur la touche ENTRÉE. Vous visualisez maintenant uniquement vos propres processus. Demandez le login de votre voisin et demandez à `top` d'afficher uniquement ses processus.
   
   Appuyez sur `f` puis sélectionnez la colonne PID, appuyez sur la touche `s`, puis `ESC` pour retourner à l'affichage. Voyez-vous ce qui a changé ? Si vous ne voyez pas le résultat, refaites la même opération en sélectionnant une autre colonne.
   
   Changez la granularité temporelle du rafraichissement en tapant `s` puis en entrant la nouvelle durée (1.0 par exemple).

9. Affichez l'arbre des processus tournant sur la machine avec la commande `pstree`(et oui encore et toujours des arbres !)

10. Débarrassez vous de tous ces processus avec la commande `kill`.

### Entrées-sorties et redirections

> Tous les processus gèrent une table stockant le nom des différents fichiers qu’ils utilisent. Chaque index de cette table est appelé un descripteur de fichiers. Par convention les trois premiers descripteurs correspondent à :
> 
> * **l’entrée standard** - descripteur 0 : si le programme exécuté par le processus a besoin de demander des
>    informations à l’utilisateur il les lira dans ce fichier (par défaut
>    c’est le terminal en mode lecture (i.e. le clavier)).
> * **la sortie standard** - descripteur 1 : si le programme a besoin de donner des
>    informations à l’utilisateur il les écrira dans ce fichier (par défaut
>    c’est le terminal en mode écriture).
> * **la sortie d’erreur** - descripteur 2 : si le programme a besoin d’envoyer un
>    message d’erreur à l’utilisateur il l’écrira dans ce fichier (par
>    défaut c’est le terminal en mode écriture).
> 
> Il est possible de rediriger ces entrées-sorties vers n'importe quel type de fichier au moyen de la syntaxe suivante :
> 
> | notation     | effet                                                                                                         |
> | ------------ | ------------------------------------------------------------------------------------------------------------- |
> | `n<fichier`  | redirige en **lecture** le descripteur *n* sur *fichier*                                                      |
> | `n>fichier`  | redirige en **écriture** le descripteur *n* sur *fichier*                                                     |
> | `n>>fichier` | redirige en **écriture** le descripteur *n* sur *fichier* en **concaténant** à la fin de *fichier*            |
> | `n<&m`       | copie le descripteur *m* sur le descripteur *n* en **lecture** i.e. *n* et *m* correspondent au même fichier) |
> | `n>&m`       | copie le descripteur *m* sur le descripteur *n* en **écriture**                                               |
> | `n<<marque`  | redirige en **lecture** le descripteur *n* jusqu'à ce que *marque* soit lu                                    |
> 
> En pratique on utilise le plus souvent :
> 
> * `< fichier` pour rediriger l'entrée standard depuis le fichier
> * `> fichier` pour rediriger l'entrée standard vers le fichier

#### Exercices

Pour cette série d'exercices (et les suivantes), vous pouvez travailler indifféremment sur la machine utilisée dans la première séance (`195.220.223.226`) ou sur la machine accessible avec vos comptes universitaires (`cartan.campus.univ-poitiers.fr`).

*(oui, je n'ai pas le même login sur les deux machines, ce serait trop simple)*

1. Récupérez le répertoire `redirections` dans le répertoire `~alayrang/fichiers_diu/`. Puis, placez vous dans le répertoire .

2. Utilisez la commande `cat fich1 fich150`. Relancez la commande en
   redirigeant les erreurs dans le fichier `erreurs`.

3. Ajouter la ligne suivante au fichier `fich2` en utilisant la
   commande `cat` :
   
   ```
   ligne ajoutée en fin de fichier
   ```

4. Mettez le résultat de la commande `history` dans le fichier
   `mon_travail`.

5. Consultez le contenu du fichier `fich3`. La commande `minuscule`
   transforme du texte en minuscules. Utilisez la pour convertir le
   contenu du fichier `fich3`. Le résultat de la conversion devra se
   trouver dans le fichier `fich4`

### Les filtres

Il est possible de combiner deux commandes en "branchant" la sortie
standard d'une première commande sur l'entrée standard d'une seconde
au moyen d'un **tube**. La syntaxe est la suivante :

```
  commande1 | commande2 | commande3 | ...
```

Les **filtres** sont des commandes qui lisent des données sur leur
entrée standard, les modifient et écrivent le résultat sur leur sortie
standard. Voici les principaux filtres :

| commande | effet                                                                                                                                                                      |
| -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `cat`    | retourne les lignes lues sans modification.                                                                                                                                |
| `cut`    | ne retourne que certaines parties de chaque lignes lues.                                                                                                                   |
| `grep`   | retourne uniquement les lignes lues qui correspondent à un modèle particulier ou qui contiennent un mot précis.                                                            |
| `head`   | retourne les premières lignes lues.                                                                                                                                        |
| `more`   | retourne les lignes lues par bloc (dont la taille dépend du nombre de lignes affichables par le terminal) en demandant une confirmation à l’utilisateur entre chaque bloc. |
| `sed`    | édite le texte lu en fonction de commandes.                                                                                                                                |
| `sort`   | trie les lignes lues.                                                                                                                                                      |
| `tail`   | retourne les dernières lignes lues.                                                                                                                                        |
| `tee`    | envoie les données lues sur la sortie standard ET dans un fichier passé en paramètre.                                                                                      |
| `tr`     | remplace des caractères lus par d’autres.                                                                                                                                  |
| `uniq`   | supprime les lignes consécutives identiques.                                                                                                                               |
| `wc`     | retourne le nombre de caractères, mots et lignes lus.                                                                                                                      |

#### Exercices

1. Placez vous dans le répertoire `~alayrang/fichiers_diu/filtres`
2. Utiliser la commande `wc` pour connaître le nombre de lignes du
   fichier (`man wc` pour savoir comment l'utiliser)
3. Affichez la première ligne du fichier pour avoir une idée du
   contenu.
4. Rechercher les informations concernant un maire français de votre choix dans le
   fichier (la commande `grep`sera utile).
5. Comptons le nombre de femmes et d'hommes...
   + Tout d'abord nous devons récupérer le champ "code sexe" (commande
     `cut`)
   + Essayez ensuite de voir ce que l'on peut faire avec la commande
     `uniq`.
   + Quelle commande ajouter pour arriver à nos fins ?
   + Bonus 1 : on peut enlever la première ligne inutile ?
   + Bonus 2 : Ça serait plus sympa si on affichait hommes et femmes
     plutôt que H et F (voir la commande `sed`)

## Variables d'environnement et substitutions

### Substitutions et jokers

> Quand vous tapez une ligne de commande, celle-ci subit un certain
> nombre de transformations avant d'être exécutée :
> 
> 1. Développement des variables : elles sont remplacées par leur valeur
> 2. substitution de commande : on peut remplacer une commande par sont
>    résultat
> 3. Découpages des mots (espaces, tabulations)
> 4. Développement des chemins de fichiers
> 
> Ces transformations se basent sur des caractères spéciaux qui ont un
> sens pour le shell : `espace, tabulation, | & ; ( ), < >, $, ~, ‘, * ?
> [], ^, -, {}`
> 
> On peut empêcher l'interprétation de ces caractères avec des
> protections:
> 
> + de caractère avec `\`
>   
>   ```
>       $ echo \$HOME
>       $HOME
>   ```
> 
> + complète avec des quotes `' '`
>   
>   ```
>       $ echo '~ et $HOME représentent la même chose'
>       ~ et $HOME représentent la même chose
>   ```
> 
> + protection simple : les caractères \, $ et `` ` `` sont interprétés
>   mais pas les autres
> 
> ```
>       $ echo "~ et $HOME représentent la même chose"
>       ~ et /home/petery représentent la même chose
> ```
> 
> Pour remplacer une commande par son résultat (substitution de
> commande), on utilise une des syntaxes suivantes : `` `commande` `` ou `$(commande)`
> 
> ```
>         $ echo mon login est $(whoami)
>         mon login est petery
> ```
> 
> Un ensemble de méta-caractères (jokers) permet de spécifier des
> modèles de noms de fichiers. Le shell remplace ces modèles par les
> fichiers correspondants (substitution de fichiers).
> 
> + `*` remplace une suite quelconque de caractères (éventuellement vide)
> + `?` remplace un et un seul caractère quelconque
> + `[liste]` remplace n'importe quel caractère de *liste*
>   + `[^liste]` remplace n'importe quel caractère **pas** dans la
>     liste
>   + `[car1-car2]` remplace les caractères contenus dans l'intervalle
>     car1 - car2

#### Exercices

1. Placez vous dans le répertoire `~alayrang/fichiers_diu/substitutions`

2. Afficher la liste des fichiers
+ dont le nom commence par un `f`
+ dont le nom possède quatre lettres et qui ont un `t` en première et troisième lettre
+ dont le nom a exactement quatre lettres
+ dont la première lettre n'est pas une minuscule

### Les variables d'environnement

Les variables d'environnement modifient le fonctionnement du shell ou
de certaines commandes. Si vous voulez voir les commandes définie pour
votre shell, utilisez la commande `env`. On peut accéder au contenu
d'une variable avec la syntaxe `${nom_variable}`

Une variable peut être définie par affectation en utilisant la syntaxe
`nom_variable=valeur` (sans espaces).

Une variable définie dans un shell n'est valable que dans ce
shell. Pour qu'elle soit connue des shells fils, il faut utiliser la
commande `export`.

#### Exercices

1. Affichez le contenu de la variable PWD avec la commande `echo`.

2. Affichez le contenu de la variable HOME, à quoi cela correspond ?
    Petite expérience : sauvegardez temporairement le contenu de votre
    variable d'environnement HOME dans une autre variable
    d'environnement, en exécutant par exemple dans votre interpréteur `OLDHOME=$HOME`.
   
    Modifiez maintenant la variable d'environnement HOME en
    lui affectant la valeur `/home/DIU/votre\_login/system`. 
   
    Exécutez la commande `cd`puis la commande `cd ~`. Que se passe-t-il ?
   
    Repositionnez votre variable HOME à sa valeur initiale : `HOME=$OLDHOME`

3. La variable PATH contient les chemins parmi lesquels le shell
   cherche la commande tapée. Ces chemins sont séparés par ":"
   
   + Affichez le contenu de la variable PATH
   + Essayez de lancer la commande `salut` que vous avez écrite précédemment.
   + Modifiez le PATH de manière à pouvoir lancer cette commande de
     n'importe où.
   + Tapez la commande `xterm` pour obtenir un nouveau
     terminal. Placez vous dans celui-ci et taper la commande
     `salut`. Est-ce que cela fonctionne ? Corrigez cela et
     recommencez pour vérifier.
   4. Avec la commande `set`, affichez l'ensemble des variables présentes dans votre environnement. Regardez plus particulièrement les variables `HOME`, `HOSTNAME`, `LOGNAME`, `COLUMNS`, `LINES`, `HISTFILE`, `UID`, `USER` et comparez leurs valeurs avec celles de vos voisins.

# Pour aller plus loin...

## Droits

----------------------

Fichiers et répertoires ont des droits de création par défaut. La
commande [`umask`](https://fr.wikipedia.org/wiki/Umask) permet de
visualiser et de modifier ces droits.

------------------------- 

6. Visualisez le masque actuellement appliqué. Déduisez en les droits
   obtenus pour les répertoires et les fichiers.
7. Monsieur Optimiste souhaite donner les droits maximum à tout le
   monde sur ses fichiers et répertoires. Modifier le umask en
   conséquence et testez (avec `touch fichier` et `mkdir repertoire`).
8. Monsieur Parano ne veux laisser aucun droit aux membres du groupe et
   aux autres. Modifier le umask en conséquence et testez.

## Les scripts

**Valeur de retour d'une commande** 

Une commande renvoie une valeur de retour. Par convention elle renvoie
0 si la commande s'est bien passée et une autre valeur en cas de
problème. Le code de retour de la commande est accessible avec `$?`.
On peut utiliser cela pour combiner deux commandes avec `&&` (ET) et
`||` (OU) :

```
    $ ls /tmp 2> /dev/null && echo le répertoire existe
    le répertoire existe
    $ ls /toto 2> /dev/null && echo le répertoire existe

    $ ls /toto 2>/dev/null || echo "le répertoire n'existe pas"
    le répertoire n'existe pas
    $ ls /tmp 2>/dev/null || echo "le répertoire n'existe pas"
```

**Paramètres de position**

Le nom d'une commande et ses paramètres sont disponibles pour un script shell
avec des noms de variables spécifiques :

+ $0 correspond au nom de la commande
+ $1...$9, ${10}--${`nn`} désigne les différents paramètres de la
  commande
+ $* correspond à l'ensemble des paramètres (à partir de $1)
+ $# donne le nombre de paramètres de la commande

La commande `shift` permet de décaler les paramètres vers la gauche
(i.e., $0, prend la valeur de $1, $1 prend la valeur de $2,
etc.). Cela influe sur le nombre de paramètres.

### Instruction conditionnelle

La syntaxe de l'instruction conditionnelle est la suivante :

```
    if <commande>
        then
            ...
    elif <commande>
        then
            ...
        else
            ...
    fi
```

L'instruction `if` (et `elif`) exécute la commande et vérifie sa
valeur de retour. Si la valeur retournée est vraie (0) on rentre dans
le `then` sinon dans le `else`

### Boucles

**Boucle *for***

La syntaxe est la suivante :

```
    for variable in <liste>
    do
        ...
    done
```

La boucle *for* itère sur une liste. À chaque itération, la variable
prend la valeur d'un des éléments de la liste. La liste peut être
définie à partir d'une substitution de chemins (méta-caractères) ou de
commande.

**Boucle *while***

La syntaxe est la suivante :

```
    while <commande>
    do
        ...
    done
```

La boucle *while* exécute la commande et vérifie sa valeur de
retour. Si la valeur retournée est vraie (0) on rentre dans la boucle
sinon on passe à la suite.

------------------------------------

Bash offre d'autres structures de contrôle qui sont documentées dans
la man page de bash.

------------------------------------

#### Exercices

1. Placez vous dans le répertoire `scripts`
2. Éditez le fichier `mesParametres` et complétez les éléments
   manquants. Testez le.
3. Ecrire un script nommé `existe` qui prend en paramètre un nom de
   fichier. la commande affiche "le fichier existe" si le nom est
   correct et "le fichier n'existe pas" dans la cas contraire. Vous
   utiliserez la commande `ls` pour vérifier l'existence du fichier.
4. Modifiez cette commande pour quelle prenne en paramètre plusieurs
   nom et teste l'existence de chacun des noms.

-----------------------------------

Une commande fréquement utilisée pour les tests est la commande `test`
qui permet d'effectuer des comparaisons sur des nombres ou des chaînes
de caractères. Elle permet également de vérifier les propriétés des
fichiers et répertoires.

-------------------------

5. Modifiez votre script pour utiliser la commande `test` au lieu de
   `ls`
6. Écrire un script nommé `compteFichiers.sh` qui prend en paramètre
   un ensemble de noms de répertoires.
   + Si aucun paramètre n'est passé, le script affiche le message
     d'erreur "Vous devez passer des noms de répertoires en
     paramètres"
   + Pour chacun des paramètres :
     + Si le nom ne correspond pas à un fichier, le script affiche
       "*nom_fichier* n'existe pas"
     + Si le nom ne correspond pas à un répertoire, le script  affiche
       le message d'erreur : "*nom_fichier* n'est pas un répertoire"
     + Si le nom correspond à un répertoire le script affiche :
       "Le *nom_repertoire* contient *n* fichiers réguliers et *m*
       sous-répertoires*"
   + S'il y a un problème avec les paramètres (affichage d'un
        message d'erreur), alors la script s'interrompt immédiatement
        et renvoie un résultat faux (différent de 0). Dans le cas
        contraire il renvoie vrai (voir la commande `exit` du shell).

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)
