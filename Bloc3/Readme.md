Bloc 3 - Architectures matérielles et robotique, systèmes et réseaux
====================================================================

DIU Enseigner l'informatique au lycée

Univ. Poitiers, Canopé, AEFE


* Initiation à UNIX, à l'interpréteur de commandes
  * [support de travaux pratiques, shell.md](./shell/shell.md)
	([au format PDF](./shell/shell.pdf))
* Système d'exploitation 
  * _Processus, système de fichiers, shell_  
  [support de présentation](./slides-psfs/Readme.md)

* Architecture des ordinateurs
  * [M999 le processeur débranché](m999/Readme.md)
  *	[La pile débranchée](https://framagit.org/infosansordi/pile-debranchee/blob/master/Readme.md)
  * _Architecture des ordinateurs_  
	[support de présentation PDF](./slides-archi/archi-slide.pdf)
  * [Circuits et portes logiques](./circuits/Readme.md)	
	
