---
title: Circuits logiques
subtitle : Bloc 3
author: |
    | DIU Enseigner l'informatique au lycée
date: octobre 2019
---


Circuits logiques
=================

**Objectifs**

- voir (ou revoir) les fonctions booléennes et les portes logiques
- réaliser une opération élémentaire (addition)
- utiliser un logiciel de simulation de circuits logiques

Fonctions booléennes, portes logiques
======================================

Cette section résume les prérequis nécessaires.

→ Algèbre de Boole
------------------

Fondamentaux. 

* Ensemble de deux éléments, notés 0 et 1
* Ensemble de trois opérateurs élémentaires : ET, OU, NOT  
  - ET est aussi noté _AND_, •, ou $`\land`$,  
  - OU est aussi noté _OR_, $`+`$, ou $`\lor`$,  
  - NON est aussi noté _NOT_, ou $`\lnot`$, d'un élément suivi d'une
    quote $`'`$,  ou encore surmonté d'un trait $`\overline{\phantom{m}}`$ 
* Postulats, ou axiomes (indémontrables) :
  1. Variable logique: A = 0 si A ≠ 1, et A = 1 si A ≠ 0
  1. NON : si A = 0, alors A' = 1 ; et si A = 1, alors A' = 0
  1. ET : 0 • 0 = 0 • 1 = 1 • 0 = 0, et 1 • 1 = 1
  1. OU : 0 + 0 = 0, et 0 + 1 = 1 + 0 = 1 + 1 = 1

Autres éléments. 

* Principaux théorèmes (démontrables) 

    | théorème  | opérateur ET  |  opérateur OU |
    |---------------|-------------------|--------------------|
    | Élément identité | A • 1 = A | A + 0 = A |
    | Commutativité | A • B = B • A | A + B = B + A
    | Distributivité | A•(B+C) = A•B + A•C | A+(B•C) = (A+B) • (A+C) |
    | Complémentation | A • A' = 0 | A + A' = 1 |
    | Idempotence | A • A = A | A + A = A |
    | Constante | A • 0 = 0 | A + 1 = 1 |
    | Double négation |A'' = A ||
    | Associativité | A • (B • C) = (A • B) • C | A + (B + C) = (A + B) + C |
    | Absorption | A • (A + B) = A | A + A • B = A |
    | De Morgan |(A • B)' = A' + B' | (A + B)' = A' • B' |

    On notera les lois de De Morgan très utiles pour vérifier les
    conditions de fin d'itération. 

	Les démonstrations de l'un ou l'autre des propriété peuvent faire
    l'objet d'exercices. 

Table de vérité.

* Une fonction booléenne $`f(x1, x2, \ldots xn)`$ de $`n`$ variables booléennes résulte en une valeur booléenne (0 ou 1) pour chacune des $`2^n`$ combinaisons possibles de valeur des variables $`x1, x2, \ldots xn`$.

* La _table de vérité_ d'une fonction $`f`$ : on énumère les $`2^n`$
  combinaisons de 1 et de 0 possibles pour les valeurs des $`n`$
  variables, et on indique la valeur de $`f`$ pour chacune de ces
  combinaisons :

|   | $`x1`$ | $`x2`$ | $`\ldots`$ | $`xn`$ | $`f(x1, x2, \ldots xn)`$ |
|---|----|----|-----|----|-----|
|0  | 0 | 0 | 0 | 0 | ? |
|1  | 0 | 0 | 0 | 1 | ? |
|2  | 0 | 0 | 1 | 0 | ? |
|.  | . | . | . | . | ? |
|$`2^{n-1}`$  | 1 | 1 | 1 | 1 | ? |

* Il y a $`2^{2^n}`$ fonctions booléennes à $`n`$ variables.

  Peut faire l'objet d'un exercice. Commencer par une, puis deux variables. 

* Un ensemble est dit _fonctionnellement complet_ si et seulement si toute expression peut être décrite à l'aide des opérations de ces ensembles.
    - l'ensemble {ET, OU, NON} est fonctionnellement complet.
    - l'ensemble {NON-ET} aussi
    - l'ensemble {NON-OU} aussi

  Peut faire l'objet d'exercices. 

→ Portes logiques
-----------------

* Les portes logiques réalisent les implémentations matérielles des
  opérations booléennes.
* On utilise une représentation schématique indiquant ces opérations
  (cette représentation schématique est normalisée ; une représentation
  alternative à base exclusivement de rectangles, elle aussi
  normalisée est parfois rencontrée).

<div >
<table>
<thead><tr><th>opération</th><th>symbole</th><th>table de vérité</th></tr></thead>
<tr>
<td>NON</td>
<td><img src="figs/NOT_ANSI_Labelled.svg"/></td>
<td>

| A | Q |
|---|---|
| 0 | 1 |
| 1 | 0 |

</td>
</tr>
<tr>
<td>ET</td>
<td><img src="figs/AND_ANSI_Labelled.svg"/></td>
<td>

| A | B | Q | 
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

</td>
</tr>
<tr>
<td>OU</td>
<td><img src="figs/OR_ANSI_Labelled.svg"/></td>
<td>

| A | B | Q | 
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

</td>
</tr>
</table>
</div>

→ Réalisation physique : transistors
------------------------------------

* La réalisation physique des portes logiques repose actuellement sur
  l'utilisation de transistors en mode commutation
  (passant/bloquant). 
* Il existe d'autres dispositifs plus manuels pour réaliser des portes
  logiques 
  - des [poulies](https://www.alexgorischek.com/project/pulley-logic-gates)
  - des [poulies en impression 3D](https://www.thingiverse.com/thing:1720219)
  - des [dominos](https://youtu.be/lNuPy-r1GuQ)
  - des [billes sur un plateau basculant](https://youtu.be/H6o9DTIfkn0)
	ou [DOI: 10.4230/LIPIcs.SOCG.2015.16](http://dx.doi.org/10.4230/LIPIcs.SOCG.2015.16)

Réaliser une addition binaire avec des circuits logiques
========================================================

Nous supposons simplement disponibles les portes ET, OU et NON.

→ Additionner deux nombres binaires de 1 bit
--------------------------------------------

* Soient 2 nombres binaires de 1 bit notés A et B : on souhaite
  calculer leur somme S = A + B en base 2 :
  - si A et B valent 0, S vaut 0,
  - si A ou B valent 1, S vaut 1,
  - et si A et B valent 1, S vaut 0 mais  il y a une retenue R (qui vaudra donc 1 uniquement dans ce cas).

* Nous avons donc à calculer 2 fonctions logiques, une pour S et une pour R
  - entrées: A et B, sorties : S la somme, et R la retenue

* Tables de vérité de S et de R

| A | B | S | R |
|---|---|---|---|
| 0 | 0 | 0 | 0 |
| 0 | 1 | 1 | 0 |
| 1 | 0 | 1 | 0 |
| 1 | 1 | 0 | 1 |

* Il faut construire les circuits calculant respectivement S et
  R à l'aide de portes logiques: c'est la synthèse avec portes
  logiques 
  - la retenue R est un ET (cela se voit, comparez les tables de
	vérité)  
    → donc R = A•B
  - la somme S est vraie si et seulement si (A est faux ET B est vrai)
    OU (A est vrai ET B est faux)  
	→ donc S = A'•B + A•B' (on peut reconnaître le OU EXCLUSIF, _XOR_).

* Notons que pour exprimer S, nous avons simplement
  - retenu les lignes de la table de vérité où S est vrai,
  - calculé l'expression correspondante à chaque ligne (ET des
	variables d'entrée, éventuellement complémentées),  et enfin
  - réalisé un OU de toutes ces expressions.

  C'est la forme appelée _somme de produits_ de S.

* Il reste à réaliser le schéma avec des portes logiques. 

* Ce circuit se nomme un _demi-additionneur_ (_half-adder_ en anglais)

→ Additionner deux nombres binaires de 1 bit avec retenue entrante
------------------------------------------------------------------

* Il s'agit maintenant de réaliser un additionneur sur 1 bit, avec une
  retenue entrante 
  - en entrées : A, B, et C la retenue entrante
  - en sorties : S la somme, et R la retenue sortante

* Comme précédemment, il est nécessaire de construire les tables de
  vérités pour les 2 fonctions S et R.  R
  - remarquons que lorsque la retenue entrante C vaut 0, la table de
	vérité est identique à celle ci-dessus.  
	On propose donc de construire la table en utilisant l'ordre C, A,
    B, la moitié du travail est ainsi déjà faite. 

| C | A | B | S | R |
|---|---|---|---|---|
| 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 1 | 0 |
| 0 | 1 | 0 | 1 | 0 |
| 0 | 1 | 1 | 0 | 1 |
| 1 | 0 | 0 |   |   |
| 1 | 0 | 1 |   |   |
| 1 | 1 | 0 |   |   |
| 1 | 1 | 1 |   |   |

* Construisons les circuits calculant respectivement S et R à l'aide
  de portes logiques 

* Ce circuit est un _additionneur complet_ (_full-adder_ en anglais)

→ Additionner deux nombres binaires de 4 bits
---------------------------------------------

* Il est aisé de réaliser l'addition de 2 nombres A et B de 2 bits si
  l'on dispose de 2 additionneurs 1 bit complets.
  - on utilise un additionneur pour additionner chaque bit des opérandes A et B, et on fait se propager la retenue en cascadant la retenue sortante d'un additionneur avec la retenue entrante de l'additionneur suivant.

* Le schéma suivant illustre ce principe.

  ![](figs/adder2bits.png)

* Construisons un additionneur de 2 nombres sur 4 bits, soit à l'aide
  de 4 additionneurs complets de 1 bit, soit à l'aide de deux
  additionneurs 2 bits. 

→ Conception d'une UAL - Unité arithmétique et logique
------------------------------------------------------

* Dans un processeurs, l'UAL – Unité arithmétique et logique, _ALU_ en
  anglais – désigne l'ensemble des circuits effectuant les opérations
  élémentaires définies par le langage d'assemblage.

* Les opérations logiques, et les décalages se réalisent directement
  avec des portes logiques ou de simples connexions. 

* Les opérations arithmétiques utilisent au plus un additionneur
  binaire :
  - soustraction: opérande passée en complément à 2 (complément à 1 (NOT) + 1
    (en retenu entrante))
  - multiplication/division : "séquence"
    d'additions/soustractions/décalages, ou réduction additive de
    produits binaires (AND) partiels. Certains microcontrôleurs ne
    disposent pas de ces opérations car elles sont complexes. 

* Toutes les opérations élémentaires sont calculées en permanence et
  en parallèle. 

* Un seul résultat est conservé en fonction de l'instruction exécutée
  - la sélection du résultat est réalisée avec un multiplexeur

Logiciel de simulation de circuits Logiques
===========================================

* Nous utiliserons les logiciel (libre) _logisim_, plus
  particulièrement la version _logisim-evolution_ disponible à
  [github.com/reds-heig/logisim-evolution](https://github.com/reds-heig/logisim-evolution).
* Logisim est un outil pédagogique de conception et de simulation de circuits logiques numériques.

> Des alternatives à logisim sont proposées en fin de document. 

→ Installation
--------------

* Logisim, et logisim-evolution, est écrit en Java
  - il est donc multiplateforme, Linux/macOS/Windows
  - il ne nécessite que l'installation préalable de Java
	(version [Oracle](https://www.java.com/fr/download/) propriétaire
    ou [OpenJDK](https://jdk.java.net/) GPL). 

* L'installation consiste à télécharger l'archive de l'application
    [`logisim-evolution.jar`](http://reds-data.heig-vd.ch/logisim-evolution/logisim-evolution.jar) 

* L'application s'exécute directement ou depuis un terminal de commandes
  ```
  java -jar logisim-evolution.jar
  ```

→ Prise en main
---------------

* Logisim présente, en plus des menus et boutons d'outils, trois zones
  principales : 
  - à droite la zone de dessin de schéma avec une grille de placement
    des objets 
  - à gauche un liste hiérarchique (arbre) des objets que l'on peut
    placer dans la grille 
  - sous cette liste, une zone pour éditer les propriétés de l'objet
	sélectionné  

  ![](figs/prise-en-main.png)
	
* Sous le menu, à gauche, les 2 boutons importants sont
  - la flèche pour sélectionner un objet dans le schéma ou relier les
	objets par des fils, et
  - la main pour modifier les valeurs/actionner les boutons...

* La simulation du circuit est par défaut active en permanence (voir
  menu _Simulation_ > _Simulation enclenchée_). 

* Reproduisez le schéma donné en exemple en modifiant les propriétés
  des objets présentés sur la figure.

* Les fils verts clairs ou verts foncés indiquent que le fils comporte
  la valeur 0 ou 1. 

* Les fils noirs indiquent que le fils comporte plusieurs bits, c'est
  un bus
  - un composant _Séparateur (Splitter)_ de _Cablage_ permet
    d'effectuer les conversions dans les deux sens : de plusieurs fils
    vers bus, et de bus vers plusieurs fils.
  - un fil orange indique une incompatibilité de largeurs de bus entre
    les composants connectés

* En mode simulation (la main), en cliquant sur une entrée, la valeur
  alterne entre 0 et 1. 

* Pour les entrées/sorties des circuits, on utilisera de préférence
  des pins car ils permettent de réaliser des circuits
  hiérarchiques. On utilisera 
  - des pins pour les entrées
  - des pins avec la propriété _Sorties? Oui_ 

  Seul le schéma principal de la hiérarchie comportera tous les
  dispositifs d'entrées/sorties si nécessaires.

→ Demi-additionneur
-------------------

* Il s'agit de réaliser le circuit suivant
  - A et B sont des pins en entrée,
  - S et R des pins en sortie,
  - il sera possible de réutiliser ce circuit dans un autre circuit,
    ces pins étant alors accessibles dans le niveau supérieur 

  ![](figs/halfadder.png)

<!--

* Nous avons ajouté une horloge `sysclk` qui n'est pas connectée :
  elle est seulement nécessaire pour réaliser des chronogrammes de
  circuit
  - il s'agit d'un composant _Horloge_ de _Cablage_
  - ses propriétés _Durée haute_ et _Durée basse_ sont toutes deux
    définies à un coup d'horloge
  - il ne faut le connecter à aucun composant
  - on produit un chronogramme depuis le menu _Simulation_ >
	_Chronogramme..._ 

    ![](figs/halfadder-chrono.png)

* Sur ce chronogramme, il est possible de vérifier que l'additionneur
  1 bit est correct 

--> 

* Vérifions la correction de cet additionneur en fournissant en entrée
  les différentes valeurs possibles de A et B

→ Additionneurs complets 1 bit et 4 bits
----------------------------------------

* On réalise un nouveau composant "additionneur complet 1 bit" en
  utilisant le composant "demi-additionneur" que nous venons de
  définir.

* On réalise un additionneur 4 bits à l'aide de cet additionneur
  complet.

> Le répertoire [`logisim/`](./logisim) contient les fichiers logisim
solution des manipulations proposées. 

Pour aller plus loin
====================

Vous pouvez accessoirement (_i.e. hors programme NSI_) :

* voir la réalisation physique des portes logiques faite à l'aide de
  transistors (à venir)
* voir la réalisation de circuits séquentiels à l'aide de portes
  logiques (à venir)
* réaliser un registre avec des bascules et construire
  un compteur (à venir)
* étudier la réalisation d'une unité de contrôle avec l'exemple du
  processeur M999 (à venir)


Références et remerciements
===========================

* Remerciements à Jean-Luc Levaire pour une version initiale de cette
  activité. 

* Schémas des portes logiques, domaine public, source Wikimedia 
  - [commons.wikimedia.org/wiki/File:NOT_ANSI_Labelled.svg](https://commons.wikimedia.org/wiki/File:NOT_ANSI_Labelled.svg)
  - [commons.wikimedia.org/wiki/File:AND_ANSI_Labelled.svg](https://commons.wikimedia.org/wiki/File:AND_ANSI_Labelled.svg)
  - [commons.wikimedia.org/wiki/File:OR_ANSI_Labelled.svg](https://commons.wikimedia.org/wiki/File:OR_ANSI_Labelled.svg)

* Logiciels pédagogiques de conception et simulation de circuits
  - logisim-evolution, multiplateforme, téléchargement à [github.com/reds-heig/logisim-evolution](https://github.com/reds-heig/logisim-evolution)
  - CircuitVerse, utilisable en ligne à  [circuitverse.org/](https://circuitverse.org/)
