Contenu affiché sur M@gistère
=============================

(et autres infos)

Textes Markdown de contenu M@gistère
====================================

Ressources bloc 3
-----------------

→ en ligne à https://magistere.education.fr/dgesco/mod/page/view.php?id=81551


Les ressources du bloc sont hébergées (pour le moment) sur le serveur GitLab [framagit.org/SAMDS/diu/](https://framagit.org/SAMDS/diu/).

Le point d'entrée du bloc 3 est à [framagit.org/SAMDS/diu/blob/master/Bloc3/Readme.md](https://framagit.org/SAMDS/diu/blob/master/Bloc3/Readme.md). 

On y trouve : 

* Initiation à UNIX, à l'interpréteur de commandes
  * support de travaux pratiques  
  [shell.md](https://framagit.org/SAMDS/diu/blob/master/Bloc3/shell/shell.md)
  ([au format PDF](https://framagit.org/SAMDS/diu/blob/master/Bloc3/shell/shell.pdf))
* Système d'exploitation 
  * _Processus, système de fichiers, shell_  
  [support de présentation](https://framagit.org/SAMDS/diu/blob/master/Bloc3/slides-psfs/Readme.md)

* Architecture des ordinateurs
  * [M999 le processeur débranché](https://framagit.org/SAMDS/diu/blob/master/Bloc3/m999/Readme.md)
  *	[La pile débranchée](https://framagit.org/infosansordi/pile-debranchee/blob/master/Readme.md)
  * _Architecture des ordinateurs_  
	[support de présentation PDF](https://framagit.org/SAMDS/diu/blob/master/Bloc3/slides-archi/archi-slide.pdf) 
  * [Circuits et portes logiques](https://framagit.org/SAMDS/diu/blob/master/Bloc3/circuits/Readme.md) 


Les ressources relatives aux blocs 3.4 _Systèmes embarqués_, et 3.5 _Réseaux et cryptologie_ sont accessibles par ailleurs :

* Systèmes embarqués
  * [rex.plil.fr/Enseignement/Systeme/Systeme.DIU-EIL/](https://rex.plil.fr/Enseignement/Systeme/Systeme.DIU-EIL/)

* Réseaux et cryptologie
  * via le post ad hoc du parcours sur m@gistère : [Bloc 3.5 : Réseaux et Crypto](https://magistere.education.fr/dgesco/mod/page/view.php?id=81465)

------------------------------------------------------------

Rendu travail bloc 3
--------------------

→ en ligne à https://magistere.education.fr/dgesco/mod/assign/view.php?id=81872

Déposez votre contribution

* proposition d'une séance relative au bloc 3
* thème au choix parmi ceux abordés en formation
  - indiquez à quel module 3.1 _shell_, 3.2 _système_, 3.3 _architecture_, 3.4 _système embarqué_, 3.5 _réseaux et cryptologie_, se rapporte votre travail 
* précisez comment se situe cette séance dans une séquence ou une progression sur le sujet 
* travail individuel ou en binôme
* avant la date butoir du jeudi 19 décembre 23h59

Ces ressources pourront être proposées en partage à l'ensemble des participants à la formation. Mentionnez votre opposition à ce partage si tel était le cas. 

------------------------------------------------------------


Autres info, pour mémoire
=========================

### Intervenants & thèmes bloc 3 ###

* bloc 3.1 — Initiation UNIX - 3h - Sylvie et Gilles
* bloc 3.2 - Suite système - 3h - Philippe 
* bloc 3.3 - Architecture - 6h - Philippe 
* bloc 3.4 - Systèmes embarqués - 6h - Xavier 
* bloc 3.5 - Réseaux - 6h - Laurent 

### Calendrier bloc 3 ###

lu 14 oct. pm - groupe 2 - 3.1  
ma 15 oct. am - groupe 3 - 3.1  
ma 15 oct. pm - groupe 1 - 3.1  
  
ma 15 oct. pm - groupe 2 - 3.5  
me 16 oct. am - groupe 3 - 3.5  
ve 18 oct. pm - groupe 1 - 3.5  
  
ma 22 oct. pm - groupe 3 - 3.2  
me 23 oct. am - groupe 1 - 3.2  
me 23 oct. pm - groupe 2 - 3.2  
  
lu 28 oct. am - groupe 1 - 3.3  
lu 28 oct. pm - groupe 2 - 3.3  
ma 29 oct. am - groupe 3 - 3.3  
  
lu 28 oct. am - groupe 2 - 3.4  
lu 28 oct. pm - groupe 3 - 3.4  
ma 29 oct. am - groupe 1 - 3.4  
  
ma 29 oct. pm - groupe 1 - 3.3  
me 30 oct. am - groupe 2 - 3.3  
me 30 oct. pm - groupe 3 - 3.3  
  
ma 29 oct. pm - groupe 2 - 3.4  
me 30 oct. am - groupe 3 - 3.4  
me 30 oct. pm - groupe 1 - 3.4  
  
ma 29 oct. pm - groupe 3 - 3.5  
me 30 oct. am - groupe 1 - 3.5  
me 30 oct. pm - groupe 2 - 3.5  
 
### Livret accueil ###

https://magistere.education.fr/dgesco/pluginfile.php/732541/mod_resource/content/1/FORMATION_DIPLOME_UNIV.pdf

------------------------------
