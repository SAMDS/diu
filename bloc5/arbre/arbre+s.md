---
title: arbres binaires, éléments de correction
subtitle: DIU Enseigner l'informatique au lycée, AEFE
author: Philippe Marquet
institute: Université Lille
date: juillet 2020
lang: fr
---

Trois parcours en profondeur
----------------------------


```python
def afficher_A(a):
    if not a.is_empty():
        afficher_A(a.get_left_subtree())
        print(a.get_data())
        afficher_A(a.get_right_subtree())

def afficher_B(a):
    if not a.is_empty():
        afficher_B(a.get_left_subtree())
        afficher_B(a.get_right_subtree())
        print(a.get_data())

def afficher_C(a):
    if not a.is_empty():
        print(a.get_data())
        afficher_C(a.get_left_subtree())
        afficher_C(a.get_right_subtree())
```

L'arbre donné en exemple

```python
arbre = bt.BinaryTree('F',
                bt.BinaryTree('B', feuille('A'), bt.BinaryTree('D', feuille('C'), feuille('E'))),
                bt.BinaryTree('G', VIDE, bt.BinaryTree('I', feuille('H'), VIDE)))
```

L'afficher

```python
show_tree(arbre)
```

Des exécutions possibles :

```python
>>> afficher_A(arbre)
A B C D E F G H I 
>>> afficher_B(arbre)
A C E D B H I G F
>>> afficher_C(arbre)
F B A D C E G I H 
```

Reconnaitre ces parcours :

* `afficher_A()` - parcours infixe - jaune 
* `afficher_B()` - parcours postfixe - vert
* `afficher_C()` - parcours préfixe - rouge 

Imprimer une expression
-----------------------

On imprimera une forme complètement parenthésée des expressions.

Imprimer une expression consiste donc, de manière générale, à

* imprimer une '('
* imprimer l'opérande gauche
* imprimer l'opérateur
* imprimer l'opérande droite
* imprimer ')'

Pour une feuille, l'algorithme précédent produit

* '('
* impression du "fils gauche"
* impression de la feuille
* impression du "fils droit"
* ')'

Les fils gauche et fils droit sont vides.

Ne rien imprimer pour un arbre vide produit un résultat correct.

Une traduction directe en Python est par exemple 

```python
def string_of_expr(expr) :
    if expr.is_empty(): 
            return ''
    return (" (" + string_of_expr(expr.get_left_subtree()) 
            + str(expr.get_data()) 
            + string_of_expr(expr.get_right_subtree()) + ") ")
```

De possibles améliorations sont envisageables 
* ne pas imprimer de '(' et ')' autour des feuilles
  - il suffit d'identifier les feuilles
* ne pas imprimer de '(' inutiles en fonctions de l'ordre de priorités
  des opérateurs


Évaluer une expression
----------------------

Évaluer une expression constiste à la réduire à une valeur.

Appliquer l'opérateur à ses opérandes :

* valeur de l'opérande gauche
  → un appel à évaluation 
* valeur de l'opérande droite
  → un appel à évaluation 

Pour les feuilles : 

* feuille - constante
  → sa valeur (entière) 
* feuille - variable
  → sa valeur -
  mémorisée dans une structure ad hoc

Une traduction en Python demande à préciser la manière dont sont
représentées les constantes dans les feuilles, et à la manière dont on
mémorise l'association d'une valeur au nom d'une variable.

Constante. Le nœud mémorise directement la valeur, par exemple la
valeur entière, un simple appel à la méthode `get_data()` permet de
récupérer cette valeur. 

Variable. L'utilisation d'un dictionnaire dont les clés sont les noms
des variables est particulièrement adaptée à la mémorisation de
l'ensemble des valeurs des variables. On supposera le dictionnaire
accessible dans un paramètre.

Une traduction en Python de la fonction d'évaluation est alors par
exemple

```python
def value(expr, vars) :
    if is_leaf(expr) :
        if type(expr.get_data()) == str : # a variable
            return vars[expr.get_data()]
        else :                            # a constant
            return expr.get_data()
    else :                                # an operator 
        if expr.get_data() == '+' :
            return (value(expr.get_left_subtree(), vars) 
                    + value(expr.get_right_subtree(), vars))
        elif expr.get_data() == '*' :
            return (value(expr.get_left_subtree(), vars) 
                    * value(expr.get_right_subtree(), vars))
        elif expr.get_data() == '-' :
            return (value(expr.get_left_subtree(), vars) 
                    - value(expr.get_right_subtree(), vars))
        elif expr.get_data() == '/' :
            return (value(expr.get_left_subtree(), vars) 
                    // value(expr.get_right_subtree(), vars))
```

Les expressions mais aussi les instructions - les AST
-----------------------------------------------------

Pour prolonger la réprésentation des expressions par des arbres, les
instructions d'un programme - par exemple Python - peuvent elles aussi
être représentées par des arbres.

On parle d'AST - _abstract syntax tree_ - , arbre de syntaxe
abstraite.

C'est sur cette représentation des programmes que les interprètes ou
les compilateurs travaillent.

Le module [ast](https://docs.python.org/3/library/ast.html) de Python
contient un parseur qui permet d'analyser une expression python et
d'obtenir un AST.

Raphaël CHAY nous propose :

Avec 4 lignes :
```python
import ast

expression = "a = 6 * 2"
code = ast.parse(expression)

print(ast.dump(code))
```

On obtient une imbrication d'objets montrant comment l'expression `"a = 6 * 2"` est analysée par l'interpréteur Python :

  ```python
Module(body=[Assign(targets=[Name(id='a', ctx=Store())], value=BinOp(left=Constant(value=6, kind=None), op=Mult(), right=Constant(value=2, kind=None)), type_comment=None)], type_ignores=[])
```

Même en le mettant en forme la hiérarchie est pas évidente :
  ```python
Module(
    body=[
        Assign(
            targets=[Name(id="a", ctx=Store())],
            value=BinOp(
                left=Constant(value=6, kind=None),
                op=Mult(),
                right=Constant(value=2, kind=None),
            ),
            type_comment=None,
        )
    ],
    type_ignores=[],
)
```

J'ai fait un petit script pour le visualiser avec graphviz. Le code est surtout du bidouillage sur le module `ast` et est disponible sur [pastebin](https://pastebin.com/fhb5A98N) .


Écriture polonaise inverse d'expressions
----------------------------------------

L'affichage postfixe d'un arbre représentant une expression produit son
écriture polonaise inverse.

Il s'agit de dérouler l'algorithme de parcours postfixe ou l'exécution
de la fonction suivante :

```python
def polonaise_inverse(expr) :
    if expr.is_empty(): 
        return ''
    return (polonaise_inverse(expr.get_left_subtree())  
            + polonaise_inverse(expr.get_right_subtree())
            + ' ' + str(expr.get_data())) + ' '
```

Sur l'exemple de notre expression  $6 (x+y) + (y-14)$ représentée par

```python
# 6 * (x+y) + (y−14)
expression1 = bt.BinaryTree('+',
                            bt.BinaryTree('*', feuille(6), bt.BinaryTree('+', feuille('x'), feuille('y'))),
                            bt.BinaryTree('-', feuille('y'), feuille(14)))
```

on obtient

```python
>>> polonaise_inverse(expression1)
' 6  x  y  +  *  y  14  -  + '
```

Cette forme textuelle peut être transformée en une liste de "mots",
plus facile à traiter :


```python
>>> polonaise_inverse(expression1).split()
['6', 'x', 'y', '+', '*', 'y', '14', '-', '+']
```

L'évaluation d'une telle expression peut être réalisée avec une pile :

* quand on rencontre une valeur (constante ou variable), on empile la
  valeur sur la pile, 
* quand on rencontre un opérateur, on dépile ses opérandes puis on
  empile la valeur produite. 

C'est une activité qu'il est possible de mener facilement.

Liste des étiquettes en largeur d'abord
---------------------------------------

On désire produire le résultat dans une liste Python. On utilisera
l'ajout en fin de liste.  

Pour un arbre a 

* on initialise f une file d'arbres
* on enfile a dans f

* tant que f n’est pas vide faire
  - défiler f → tête 
  - ajouter l'étiquette de la tête à la liste
  - enfiler le fils gauche dans f
  - enfiler le fils droit

Quelle terminaison de cet algorithme ?

* l'algo ci-dessus enfile des arbres vides
* il suffit de ne pas les ne pas les traiter ni les afficher quand on
  les défile 

Et donc notre algorithme devient :
[...]
* tant que f n’est pas vide faire
  - défiler f → tête
  - si tête n'est pas vide 
	  - ajouter l'étiquette de la tête à la liste
	  - enfiler le fils gauche dans f
	  - enfiler le fils droit

### avec une pile ###

Si l'on remplace l'utilisation d'une file par l'utilisation d'une
pile, on obtient un parcours postfixe à l'envers 

Il est possible de retrouver le parcours postfixe en empilant le
fils droit avant le fils gauche.

<!-- eof -->
