from list import *

def length(l):
    '''
    Return the length of a list
        
    :return: Number of elements in the list

    >>> l = List()
    >>> length(l)
    0
    >>> l = List(4,l)
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> length(l)
    3
    '''
    pass
    
def get(l, i):
    '''
    Get the element at position i (positions start at 0).
        
    :CU: not l.is_empty()
    :CU: 0 <= i < length(l) 

    >>> l = List(1, List())
    >>> get(l, 0)
    1
    >>> l = List(2, List())
    >>> l = List(4,l)
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> get(l, 3)
    2
    >>> get(l, 0)
    3
    '''
    pass
        
def search(l, e):
    '''
    Return whether e exists in the list

    :return: True iff e is an element of the list
        
    >>> l = List()
    >>> search(l, 0)
    False
    >>> l = List(4, List())
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> search(l, 3)
    True
    >>> search(l, 2)
    False
    '''
    pass
    
def string_of_list(l):
    '''
    Return a string representation of the list

    >>> l = List()
    >>> string_of_list(l)
    ''
    >>> l = List(4, List())
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> string_of_list(l)
    '3 1 4'
    '''
    pass

def python_list_of_list(l):
    '''
    Return the Python list corresponding to the list

    :return: A Python list whose length and elements are identical to `l`.

    >>> l = List()
    >>> python_list_of_list(l)
    []
    >>> l = List()
    >>> l = List(4, List())
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> python_list_of_list(l)
    [3, 1, 4]
    '''
    pass

def sorted_insert(l, x):
    '''
    Insert element x in the list.
    Return a fresh list.
    
    CU: the list must be sorted according to '<'
    
    :return: A fresh sorted List containing elements of `l` and `x`
    
    >>> l = List()
    >>> l = List(4,l)
    >>> l = List(2,l)
    >>> l = List(1,l)
    >>> sorted_insert(l, 3)
    (1.(2.(3.(4.()))))
    >>> l = List(4, List())
    >>> sorted_insert(l, 10)
    (4.(10.()))
    >>> l = List(4, List())
    >>> sorted_insert(l, 1)
    (1.(4.()))
    >>> sorted_insert(List(), 42)
    (42.())
    '''
    pass

def reverse(l):
    '''
    :return: A fresh list containing the same elements as `l` but in reversed order.

    >>> l = List()
    >>> l = List(4,l)
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> l
    (3.(1.(4.())))
    >>> r = l.reverse()
    >>> r
    (4.(1.(3.())))
    '''        
    pass
    
if __name__ == "__main__":
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=True)
    
