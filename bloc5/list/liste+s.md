---
title: listes, éléments de correction
subtitle: DIU Enseigner l'informatique au lycée, AEFE
author: Philippe Marquet
institute: Université Lille
date: juillet 2020
lang: fr
---


Chercher dans une liste
-----------------------

Une solution récursive peut être construite en considérant 3 cas

1. la liste est vide 
2. la valeur recherchée est égale à la tête
3. les autres cas - ie la valeur recherchée n'est pas égal à la tête 

...

Donc 3. la valeur recherchée n'est pas égal à la tête : 

> « savoir si la valeur recherché appartient à la liste » revient à « savoir si la valeur recherchée appartient à la liste privée de la tête »

Nous sommes dans une démarche récursive

Une possible écriture en Python. Traduction quasi directe : 
  
```python
def search(l, e):
    '''
    Return whether e exists in the list

    :return: True iff e is an element of the list
        
    >>> l = List()
    >>> search(l, 0)
    False
    >>> l = List(4, List())
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> search(l, 3)
    True
    >>> search(l, 2)
    False
    '''
    if l.is_empty() : 
        return False
    elif e == l.head():
        return True
    else:
        return search(l.tail(), e)
```

Accès à un élément d'une liste
------------------------------

Voici deux versions de `get()`, itérative et récursive.

Un version itérative basique peut être

* d'avancer dans la liste en décomptant
* en fin du décompte on a trouvé l'élément recheché

On peut ensuite prendre en compte le fait que l'indice fourni n'est
pas correct, plus grand que le plus grand que le nombre d'éléments de
la liste.

* on va donc rencontrer la fin de la liste avant la fin du décompte
* ce qu n'est pas correct

On modifie donc l'algorithme pour

* avancer dans la liste en décomptant, et tant que l'on est pas en fin 
  de liste
* il s'agit ensuite d'identifier si l'on a arréter de compter
    - parce que l'on était au bout du décompte - on avait trouvé
      l'élément, ou
    - parce que l'on a rencontré la fin de liste - l'indice founi
      n'était pas correct

Une traduction en Python peut être la suivante.

On a choisi de renvoyer `None` si l'indice était au delà des indices
possibles. 

```python
def get_i (l, i):
    '''
    Get the element at position i (positions start at 0).
        
    :CU: not l.is_empty()
    :CU: 0 <= i < length(l) 

    >>> l = List(1, List())
    >>> get_i(l, 0)
    1
    >>> l = List(2, List())
    >>> l = List(4,l)
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> get_i(l, 3)
    2
    >>> get_i(l, 0)
    3
    '''
    while not l.is_empty() and i != 0:
        i -= 1
        l = l.tail()
    if l.is_empty():
        return None 
    return l.head()
```

Une première version récursive peut être construite en identifiant
deux cas : 

* l'indice est nul, l'élément recherché est la tête de liste
* sinon, il s'agit de considérer la recherche de l'élément dans la
  liste privée de sa tête.
  L'indice de ceet élément est de 1 inférieur à son indice dans la
  liste initiale.

Voici une possible traduuction en Python :

```python
def get_r (l, i):
    '''
    Get the element at position i (positions start at 0).
        
    :CU: not l.is_empty()
    :CU: 0 <= i < length(l) 

    >>> l = List(1, List())
    >>> get_r(l, 0)
    1
    >>> l = List(2, List())
    >>> l = List(4,l)
    >>> l = List(1,l)
    >>> l = List(3,l)
    >>> get_r(l, 3)
    2
    >>> get_r(l, 0)
    3
    '''
    if i == 0:
        return l.head()
    else:
        return get_r(l.tail(), i-1)
```

Une seconde version récursive devrait considérer le cas où l'indice
est en dehors des limites possibles.

On aura alors après quelques appels récursifs, à faire à une recheche
dans une liste vide. Il s'agit donc simplement d'identifier ce cas et
renvoyer une valeur _ad hoc_. 



Insérer un élément dans une liste triée
---------------------------------------

Une petite aide pour une version récursive.

Distinguer 3 cas :

1. la liste est vide
2. l'élément à insérer est inférieur à la tête de liste
3. les autres cas - ie l'élément à insérer est supérieur à la tête de liste

Renverser une liste
-------------------

Construire la liste renversée, on considère le reste de la liste
à renverser et la liste construite à partir des premiers éléments
renversés.

On définit un algorithme annexe qui produit la liste renversée
à partir de ces deux listes.

Si la liste à renverser est vide. On a terminé, le résultat est la
liste des éléments déjà renversés. 

Sinon, on considère le 1er élément de la liste à renverser que l'on va
changer de liste :

* la nouvelle liste des éléments à renverser est l'ancienne valeur de
  cette liste, privée de sa tête
* la nouvelle liste des éléments renversés est construite à partir de
  cet élément et de l'ancienne valeur de cette liste.

Il reste à appliquer cette même méthode pour renverser les éléments
restants. Une invocation récursive de l'algorithme en construction est
possible. Un traitement itératif est également possible. 

Enfin, il reste à initier les invocations de cet algorithme :
renverser une liste consiste à renverser la liste à partir d'une liste
des éléments déjà renversés vide. 

Cet algorithme repose sur l'utilisation des primitives disponibles sur
les listes :

* isoler l'élément de tête
* isoler la liste privée de son élément de tête
* construire une liste à partit d'un élément et d'une liste
* construire un liste vide

Une traduction en Python peut être : 

```python
def reverse(l):
    return reverse2(l, List())

def reverse2 (l, rev):
    if l.is_empty():
        return rev
    else :
       rev = List(l.head(), rev)
       return reverse2(l.tail(),rev)
```

Une file avec des piles
-----------------------

Implanter une file avec des piles revient à fournir les algorithmes
deux deux primitives _enfile_ et _défiler_ (et accessoirement savoir
initialiser une file et tester si une file est vide). 

On considère deux piles

* une pile d'entrée
* une pile temporaire

Au fur et à mesure que l'on enfile une valeur

* on empile cette valeur dans la pile d'entrée

Pour défiler :

* on va chercher la valeur au fond de la pile d'entrée.
  Procédons ainsi : 
  - dépiler la pile d'entrée dans la pile temporaire
  - le dernier est l'élément recherché
  - vider la pile temporaire dans la pile d'entrée 

Cette première version peut être améliorée.
L'idée est de ne pas vider la pile temporaire dans la pile d'entrée,
mais de conserver les deux piles dans cet état.

On renomme les pile pour une meilleure compréhension. On dispose donc
de deux piles :

* une pile d'entrée
* une pile de sortie 

Enfiler : 

* c'est toujours déposer sur la pile d'entrée

Défiler :

* si pile de sortie non vide
    - l'élément recherché est en sommet de cette pile 
* sinon
    - "renverser" la pile d'entrée dans la pile de sortie

Complexité

* enfiler en $O(1)$
* défiler - première version basique 
    - nombre de valeurs dans la pile d'entrée ? 
	- min 0, max $n$ ... quel est ce $n$ ??? 
	- moyenne ...
* défiler - seconde version
    - difficile de raisonner sur une exécution de défiler. Nombre
      d'opérations dépend de la configuration des piles. 
    - on va raisonner sur l'ensemble des opérations exécutées pour une
      donnée tout au long de sa "vie, entre "son" enfiler et son
      "défiler".
	  Cette valeur est 
    	 1. empilée dans la pile d'entrée
    	 2. dépilée de la pile d'entrée
    	 3. empilée dans la pile de sortie
    	 4. dépilée de la pile de sortie
    - on a exactement 4 opérations primitives en $0(1)$ ; on reste en
      $0(1)$ 
	- on a donc "en moyenne" (on parlera plus exactement de
      "complexité amortie") une complexité en $O(1)$ pour chacune de
      nos opérations enfiler et défiler


Une pile avec des files
-----------------------

Une **première proposition** à l'aide de 2 files :

Empiler

* enfiler dans la 1re file

Dépiler

* vider la file dans la 2e file
* sauf le dernier élément qui est celui que l'on cherche
* vider la 2e file dans la 1re

Une **seconde proposition**, toujours à base de 2 files : 

* la file courante
* l'autre file

Empiler

* enfiler dans la file courante

Dépiler 

* vider la file courante dans l'autre file
* sauf le dernier élément qui est celui que l'on cherche
* l'autre file devient la courante ; et inversement


Imprimer une file
-----------------

Une petite fonction accessoire pour vérifier les résultats de
l'exécution de nos fonctions. 

```python
def file2str(f):
    '''attention, impression destructive !'''
    if f.is_empty() :
        return '>> '
    else:
        e = f.dequeue()
        return file2str(f) + str(e) + ' >> ' 
```
		
* La file vide est donc imprimée

```python
>>> file2str(Queue())
'>> '
```

* et une file de 3 éléments : 

```python
f = Queue()
f.enqueue(1)
f.enqueue(2)
f.enqueue(3)
```

```python
>>> file2str(f)
'>> 3 >> 2 >> 1 >> '
```

Renverser une file
------------------

Déroulons une proposition d'algorithme sur la file `>> 3 2 1 >>`.

Il s'agit de 

* conserver le 1er élément - `1` 
* renverser la file → `>> 2 3 >>`
* enfiler l'élément mis de côté → `>> 1 2 3 >>`

Cas d'arrêt de la récursivité :

* renverser la file vide → la file vide

```python
def reverse (f) :
    """
	Renvoie une file qui contient les éléments de la file f dans
	l'ordre inverse.
	Effet de bord : la file f est vidée. 
	"""
    if f.is_empty() :
        return Queue()
    e = f.dequeue()
    new = reverse(f)
    new.enqueue(e)
    return new 
```

et un petit test

```python
f = Queue()
f.enqueue(1)
f.enqueue(2)
f.enqueue(3)

r = reverse(f)
file2str(r)
```

Une alternative, renverser "en place"

```python
def reverse (f) :
    """Renverse les éléments de la file f"""
    if f.is_empty(): 
        return
    elem = f.dequeue()
    reverse(f)
    f.enqueue(elem)
```

Trier avec des piles
--------------------

L'idée est d'avoir 3 piles :

* la pile des valeurs non encore triées
* une pile de valeurs triées
* une pile tampon

Dans les grandes lignes

* dépiler élément par élément la pile à trier
* essayer d'insérer l'élément à la place qui lui convient dans la pile
  des éléments triés 
* pour cela déplacer tant que nécessaire les éléments du haut de cette
  pile vers la pile tampon ; placer le "nouvel" élément ; remettre les
  éléments de la pile tampon 

Initialisation : placer un élément de la "pile à trier" vers la "pile
triée" 

Des optimisations - ou complexifications - sont possibles :

* par exemple de ne pas considérer une pile des éléments triés et une
  pile tampon, mais une structure unique de 2 piles tête bêche des
  éléments triés. Le sommet des deux piles forme en quelque sorte un
  curseur pour insérer les éléments restant à trier.  

* par exemple utiliser la pile à trier comme pile tampon.

* etc.

<!-- eof -->
