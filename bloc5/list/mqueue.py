class QueueEmptyError(Exception):
    """
    Exception used by method

    * ``dequeue``

    of class :class:`Queue`.
    """

    def __init__(msg):
        super().__init__(msg)

class Queue:
    """
    a class representing a queue

    >>> q = Queue()
    >>> q.is_empty()
    True
    >>> q.enqueue(1)
    >>> q.enqueue(2)
    >>> q.is_empty()
    False
    >>> q.dequeue()
    1
    >>> q.dequeue()
    2
    >>> q.dequeue()
    """

    def __init__(self):
        """
        create a new empty queue
        """
        self.__content = []

    def is_empty(self):
        """
        :return: (bool) True si la queue est vide, False sinon
        """
        return len(self.__content) == 0

    def enqueue(self, el):
        """
        enfile un élément dans la file
        :param el: (any) un élément
        """
        self.__content.append(el)

    def dequeue(self):
        """
        défile un élément
        :return: (any) un élément
        """
        n = len(self.__content)
        if n > 0:
            res = self.__content[0]
            del self.__content[0]
            return res
        else:
            raise QueueEmptyError("empty queue")

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose = True)
