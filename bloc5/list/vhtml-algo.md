> - lorsque l'on rencontre une parenthèse ouvrante on l'empile,
- lorsque l'on rencontre une parenthèse fermante, par exemple `]` :
  -  si la pile est vide, alors l'expression est mal parenthésée - trop de fermantes,
  -  sinon, on  dépile l'ouvrante située au sommet de la pile et on vérifie que les 
        deux parenthèses correspondent
> Lorsque toute la chaîne a été parcourue, la pile doit être vide - pas d'ouvrante non fermée.
