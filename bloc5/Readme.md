DIU AEFE - juin-juillet 2020
Philippe Marquet

Cette 1re partie du bloc 5 _Algorithmique avancée_ traite des listes
et des arbres.

Et donc un menu en deux grandes sections :

Listes 
* rappels sur la structure de données abstraite liste
* algorithmes élémentaires sur les listes
* les structures de données pile et file
* algorithmique avec des listes, piles, files
* validateur HTML - mini-projet bonus 

Arbres

* se familiariser avec les arbres (binaires)
* découvrir le module `binary_tree`
* premiers algorithmes sur les arbres
* caractériser les arbres binaires 
* parcourir les arbres (binaire)
* les arbres binaires de recherche

### Bienvenue ###

Un bref message de bienvenue et une présentation très très rapide de cette 1re partie du bloc 5.

* vidéo de présentation
  - vidéo au format Matroska `0-bienvenue.mkv` - 58s

L'ensemble des ressources et leurs mises à jour sont disponibles à partir de la page [framagit.org/SAMDS/diu/tree/master/bloc5](https://framagit.org/SAMDS/diu/tree/master/bloc5)

 1re partie - les listes
------------------------

### La structure de données abstraite liste ###

Un bref rappel sur cette structure de données liste présentée dans le bloc 4. On y donne une définition formelle des listes, on propose une interface fonctionelle à cette structure de données abstraite, on prend en main l'utilisation d'une classse Python fournie. 

> Présentation à visualiser ou lire 

* vidéo de présentation sur la base d'un notebook 
  - vidéo au format Matroska `1-liste.mkv` - 17min44

* notebook support de présentation
  - [`1-liste.ipynb`](./list/1-liste.ipynb)
  - version PDF [`1-liste.pdf`](./list/1-liste.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

* fichier source 
  - classe Python `List` fournie dans [`list.py`](./list/list.py)

### Algorithmes élémentaires sur les listes ###

Une série d'exercices pour découvrir les algorithmes classiques sur les listes, parcours, transformation de listes, etc. On pourra aller jusqu'à des impléméntations sous forme de fonctions Python.

> Un premier exercice est commenté et corrigé.  
> Il est attendu que quelques exercices soient traités par chacun·e.

* vidéo de présentation sur la base d'un notebook
  - vidéo au format Matroska `2-liste-algo0.mkv` - 14min23 

* notebook support de présentation
  - [`2-liste-algo0.ipynb`](./list/2-liste-algo0.ipynb)
  - version PDF [`2-liste-algo0.pdf`](./list/2-liste-algo0.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

* fichiers source Python 
  - fichier source Python à compléter
	[`list-algo0.py`](./list/list-algo0.py)
  - fichier source Python avec éléments partiels de solutions
	[`list-algo0-sol.py`](./list/list-algo0-sol.py)

### Les structures de données pile et file ###

Une rapide présentation des piles et files.
Fourniture de classe Python pour ces structures de données.

> Présentation à visualiser ou lire. 
> Il peut être intéressant d'installer les modules Python dans son environnement de travail (qui peut être JupyterLab).

* notebook support de présentation
  - [`3-pile-file.ipynb`](./list/3-pile-file.ipynb)
  - version PDF [`3-pile-file.pdf`](./list/3-pile-file.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

* fichiers sources 
  - classes Python `Stack` fournie dans [`stack.py`](./list/stack.py)
	et `Queue` fournie dans [`mqueue.py`](./list/mqueue.py)

### Algorithmique avec des listes, piles, files ###

Une série d'exercices de conception d'algorithmes avec des listes, piles et files base d'activités que nous mènerons en classe (virtuelle).

> Travail qui sera mené en classe (virtuelle) 

* notebook support
  - [`4-liste-algo.ipynb`](./list/4-liste-algo.ipynb)
  - version PDF [`4-liste-algo.pdf`](./list/4-liste-algo.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

### Validateur HTML ###

Proposition d'un mini-projet de validateur HTML que nous évoquerons en classe (virtuelle).

> Proposition qui pourra être travaillée après la séance

* notebook support
  - [`5-vhtml.ipynb`](./list/5-vhtml.ipynb)
  - version PDF [`5-vhtml.pdf`](./list/5-vhtml.pdf)
  - fichiers de style
	[`phm.css`](./templates/phm.css),
	[`phm-nonum.css`](./templates/phm-nonum.css) 
  - élements de solution [`vhtml-algo.md`](./list/vhtml-algo.md)

* fichiers exemples et sources 
  - exemples HTML
	[`vhtml/ex1.html`](./list/vhtml/ex1.html), 
	[`vhtml/ex2.html`](./list/vhtml/ex2.html), 
	[`vhtml/ex3.html`](./list/vhtml/ex3.html), 
	[`vhtml/ex4.html`](./list/vhtml/ex4.html)
  - fichier squelette
	[`vhtml/html_parser_squel.py`](./list/vhtml/html_parser_squel.py)


 2e partie - les arbres
-----------------------

### Se familiariser avec les arbres (binaires) ###

Un bref rappel sur les arbres, la structure de données présentée dans le bloc 4. Un peu (beaucoup...) de définitions et vocabulaire.
Quelques activités sont proposées pour se familiariser avec les arbres. 

> Présentation à visualiser ou lire 

* notebook de présentation
  - [`1-arbre.ipynb`](./arbre/1-arbre.ipynb)
  - version PDF [`1-arbre.pdf`](./arbre/1-arbre.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

### Découvrir le module `binary_tree` ###

Un module Python pour manipuler des arbres binaires et les visualiser. 

> Présentation à visualiser ou lire
> Il est attendu que chacun·e installe le module dans son environnement de travail (qui peut être JupyterLab).

* notebook de présentation
  - [`2-bt.ipynb`](./arbre/2-bt.ipynb)
  - version PDF [`2-bt.pdf`](./arbre/2-bt.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

* fichier source 
  - classe Python `BinaryTree` fournie dans
	[`binary_tree.py`](./arbre/binary_tree.py)

### Premiers algorithmes sur les arbres ###

Les premiers exercices de familiarisation avec les arbres et les
premiers algorithmes.
Des éléments de correction sont fournis.

> Corrections à étudier.

* notebook support 
  - [`3-arbre-algo0.ipynb`](./arbre/3-arbre-algo0.ipynb)
  - version PDF [`3-arbre-algo0.pdf`](./arbre/3-arbre-algo0.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

* fichier source 
  - classe Python `BinaryTree` fournie dans
	[`binary_tree.py`](./arbre/binary_tree.py)

### Caractériser les arbres binaires ###

Présentation de quelques caractéristiques des arbres binaires.
Exercices et algorithmes pour caractériser les arbres.
Des éléments de corrections sont proposés. 

> Travailler les exercices de conception d'algorithmes.

* notebook support
  - [`4-arbre-algo.ipynb`](./arbre/4-arbre-algo.ipynb)
  - version PDF [`4-arbre-algo.pdf`](./arbre/4-arbre-algo.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)

### Parcourir les arbres (binaire) ###

Découverte des classiques algorithmes de parcours des arbres
(binaires).
Quelques activités et exercices associés. 

> Travail qui sera mené en classe (virtuelle) 

* notebook support
  - [`5-arbre-parcours.ipynb`](./arbre/5-arbre-parcours.ipynb)
  - version PDF [`5-arbre-parcours.pdf`](./arbre/5-arbre-parcours.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)
  - fichier image [`expression-tree.svg`](./arbre/fig/expression-tree.svg)

* fichier source 
  - classe Python `BinaryTree` fournie dans
	[`binary_tree.py`](./arbre/binary_tree.py)

### Les arbres binaires de recherche ###

Découverte des arbres binaires de recherche.
Exercices et algorithmes de recherche, insertion et suppression dans
ces arbres.
Présentation de la complexité de ces opérations.

> Ressources qui seront éventuellement travaillées en classe (virtuelle)

* notebook support
  - [`6-abr.ipynb`](./arbre/6-abr.ipynb)
  - version PDF [`6-abr.pdf`](./arbre/6-abr.pdf)
  - fichier de style [`phm.css`](./templates/phm.css)
  - fichiers image
	[abr-or-not-1.png](arbre/fig/abr-or-not-1.png) 
	[abr-or-not-2.png](arbre/fig/abr-or-not-2.png)
	[avl-rot-simple.png](arbre/fig/avl-rot-simple.png)

* fichier source 
  - classe Python `BinaryTree` fournie dans
	[`binary_tree.py`](./arbre/binary_tree.py)

------------------------------

Les notebooks proposés ici sont inspirés de ressources préparées dans le cadre du DIU Ennseigner l'informatique au lycée, novembre 2019, Univ. Lille

Équipe pédagoqique DIU EIL, Philippe Marquet, ressources éducatives libres distribuées sous [Licence Creative Commons Attribution - Partage dans les mêmes conditions 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/deed.fr) ![Licence Creative Commons (CC BY-SA 4.0)](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

------------------------------
